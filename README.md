# Hydra: Concurrent Coordination for Fault-tolerant Networking

This code executes the Hydra protocol on the STMicroelectronics STM32L433CC microcontroller driving a Semtech SX1262 radio as used on the DPP2 ComBoard.

| Folder                 | Description |
|------------------------|---------------------------|
| [*Inc*](./Inc)         | Contains the header files |
| [*Lib*](./Lib)         | Submodule containing the radio driver and message definitions |
| [*Scripts*](./Scripts) | Contains a shell script to run a simple FlockLab test |
| [*Src*](./Src)         | Contains the source files |
| [*Startup*](./Startup) | Contains the startup script for the controller MCU |
| [*Root*](./)           | Contains a STMCubeIDE project file as well as licensing and organisational files |

For more information, please visit the [Hydra wiki](https://gitlab.ethz.ch/tec/public/hydra/hydra-wiki).
