/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Communication
 *
 * This file initialises the state and implements the main event loop.
 *
 */

#include "main.h"


/* Global variables ---------------------------------------------------------*/
extern TaskHandle_t xTaskHandle_comm;


/* Static variables ----------------------------------------------------------*/
static comm_state_t comm_state;
static dcstat_t     log_cpu_dc;


/* Functions -----------------------------------------------------------------*/

void task_comm(void const* arg)
{
#if STARTUP_SLEEP_LP
    // Sleep directly after start-up, such that the start of the bootstrapping is also covered in the GPIO tracing
    comm_task_sleep_lp(STARTUP_SLEEP_LP);
#endif /* STARTUP_SLEEP_LP */

    // If DELAY_START_ENABLE is set and the code is running on FlockLab, then the node waits for DELAY_START_PIN to clear before continuing
#if DELAY_START_ENABLE && FLOCKLAB
    while (DELAY_START_PIN_VALUE) {}
#endif /* DELAY_START_ENABLE */

#if DROP_NODE_PACKETS_ENABLE
    // Delay nodes whose packets are dropped until they are released
    /*if DROP_NODE_PACKETS_FROM_ID(NODE_ID) {
        while (!DROP_NODE_PACKETS_PIN_VALUE) {}
    }*/
#endif /* DROP_NODE_PACKETS_ENABLE */

    // Init the CPU duty cycle counter
    rtos_reset_cpu_dc();
    dcstat_reset(&log_cpu_dc);

    LOG_VERBOSE("Started communication task");

    // Initialize state
    enter_bootstrapping(&comm_state, true);

#if SKIP_BOOTSTRAP
    // Skip the bootstrapping if necessary

    // Synchronize clocks virtually
    hs_timer_set_counter(0);
    update_clock_offset();
    set_drift_compensation_reference(true);
    comm_state.synced = true;

    // Go through the intermediate states of joining - receiving schedule - joined
    enter_joining(&comm_state);
#if SET_INIT_MEMBER_FLAGS
    node_id_t nr_nodes = get_nr_of_contributors(INIT_MEMBER_FLAGS);

    if (IS_FLAG_SET(INIT_MEMBER_FLAGS, NODE_ID) &&
        (nr_nodes > 1)                            ) {
        memcpy(comm_state.member_flags_e, INIT_MEMBER_FLAGS, NR_MEMBER_FLAG_BYTES);
        comm_state.sched_vers                     = 1;
        comm_state.schedule_curr[NR_SLOTS_DD - 1] = NODE_IN_SCHEDULE;
        comm_state.updated                        = false;
    } else {
        LOG_WARNING("Ignoring initial member flags since own flag is not set or is the only one");
    }
#endif /* SET_INIT_MEMBER_FLAGS */
    enter_normal(&comm_state);
#endif /* SKIP_BOOTSTRAP */

    for (;;) {

        // Execute the current slot
        switch (comm_state.next_slot.type) {
            case SLOT_TYPE_DATA_DISSEMINATION:
                SLOT_TYPE_DATA_DISSEMINATION_ON_IND();
                //LOG_VERBOSE("Data slot for %hhu", comm_state.next_slot.sender);
                run_data_dissemination_slot(&comm_state);
                SLOT_TYPE_DATA_DISSEMINATION_OFF_IND();
                break;
            case SLOT_TYPE_SCHEDULE_NEGOTIATION:
                SLOT_TYPE_SCHEDULE_NEGOTIATION_ON_IND();
                //LOG_VERBOSE("Schedule negotiation");
                run_schedule_negotiation_slot(&comm_state);
                SLOT_TYPE_SCHEDULE_NEGOTIATION_OFF_IND();
                break;
            case SLOT_TYPE_SCHEDULE_DISTRIBUTION:
                SLOT_TYPE_SCHEDULE_DISTRIBUTION_ON_IND();
                //LOG_VERBOSE("Schedule distribution");
                run_schedule_distribution_slot(&comm_state);
                SLOT_TYPE_SCHEDULE_DISTRIBUTION_OFF_IND();

                // Execute End of Round
                run_end_of_round(&comm_state);
                break;
            case SLOT_TYPE_SLEEP:
                break;
            default:
                LOG_ERROR("Invalid slot type %i", comm_state.next_slot.type);
                break;
        }

        // Add RX and TX duty cycle to corresponding phase
        rtos_add_dc_to_phase(comm_state.next_slot.type);

        // Set the earliest start time of the next slot
        comm_state.next_slot_start_time = MAX(comm_state.next_slot_start_time + SLOT_DURATION_LP, lptimer_now_corrected() + MAX_SCHEDULE_LOOKUP_TIME_LP);

        // Determine the next slot
        comm_state.next_slot = get_next_used_slot_information(comm_state.schedule_curr,
                                                              comm_state.schedule_mask,
                                                              &comm_state.next_slot_start_time);

        //uint8_t slot_number = (comm_state.next_slot_start_time + SLOT_DURATION_LP - 1UL) / SLOT_DURATION_LP % SLOTS_PER_ROUND;
        //LOG_VERBOSE("Next slot: Slot %2hhu of type %hhu for sender %3hhu", slot_number, comm_state.next_slot.type, comm_state.next_slot.sender);

        uint64_t wakeup_time = comm_state.next_slot_start_time;

        // Modify wake-up time to include the guard time
        if (comm_state.next_slot.type == SLOT_TYPE_DATA_DISSEMINATION) {
            // If the current node is the sender: Start slightly later, such that even nodes with a slower clock can receive the first message
            if (comm_state.next_slot.sender == NODE_ID) {
                wakeup_time += GLORIA_GUARD_TIME_LP;
            }
            // If it is the first slot in the round: start listening a bit earlier in case that the own clock was a bit slower
            else if (comm_state.next_slot.first_data_slot) {
                wakeup_time -= GLORIA_GUARD_TIME_LONG_LP;
            }
        }

#if !LOG_PRINT_IMMEDIATELY
        // Flush the log if enough time is left
        if (wakeup_time > (lptimer_now_corrected() + LOG_FLUSH_THRESHOLD_LP) ) {
            // Start a duty cycle counter in order to measure how much of the CPU on-time is due to log printing
            dcstat_start(&log_cpu_dc);

            // Flush the log
            uint64_t time_before = lptimer_now_corrected();
            if (!log_flush()) {
                LOG_ERROR("Failed to flush log in %llums", LPTIMER_TICKS_TO_MS(lptimer_now_corrected() - time_before));
            }

            // Check whether flushing the log took too long
            uint64_t time_now = lptimer_now_corrected();
            if (time_now < wakeup_time) {
                uint32_t time_left = wakeup_time - time_now;
#ifdef LOG_FLUSH_WARN_THRESHOLD_LP
                if (time_left < LOG_FLUSH_WARN_THRESHOLD_LP) {
                    LOG_WARNING("Finished flushing log in %2llums with only %llums left", LPTIMER_TICKS_TO_MS(time_now - time_before), LPTIMER_TICKS_TO_MS(time_left));
                } else {
#else
                {
#endif /* LOG_FLUSH_WARN_THRESHOLD_LP */
                    LOG_VERBOSE("Finished flushing log in %2llums with %4llums left",     LPTIMER_TICKS_TO_MS(time_now - time_before), LPTIMER_TICKS_TO_MS(time_left));
                }
            } else {
                LOG_ERROR("Flushing log took %llums and ended %llums too late", LPTIMER_TICKS_TO_MS(time_now - time_before), LPTIMER_TICKS_TO_MS(time_now - wakeup_time));
            }

            // Stop the duty cycle counter
            dcstat_stop(&log_cpu_dc);
        }
#endif /* !LOG_PRINT_IMMEDIATELY */

        // Wait for next slot
        comm_task_sleep_until_lp(wakeup_time);
    }
}

// Some helper functions:

inline void comm_task_sleep_lp(uint64_t duration)
{
    lptimer_set(lptimer_now() + duration, comm_task_unblock_callback);
    comm_task_block();
}

void comm_task_sleep_until_lp(uint64_t time)
{
    uint64_t now = lptimer_now_corrected();
    //LOG_VERBOSE("Going to sleep: %llums", LPTIMER_TICKS_TO_MS(now);

    if (time > (now + SLEEP_THRESHOLD_LP) ) {
        lptimer_set(corrected_to_uncorrected_lp_timestamp(time), comm_task_unblock_callback);
        comm_task_block();
    } else if (time > now) {
        // LOG_VERBOSE("Insufficient time left to use lptimer (start required in %lluus)", LPTIMER_TICKS_TO_US(time - now));
        delay_us(LPTIMER_TICKS_TO_US(time - now));
    } else {
        LOG_WARNING("Sleep until has been called %4lluus too late (expected starttime was %llums)", LPTIMER_TICKS_TO_US(now - time), LPTIMER_TICKS_TO_MS(time));
    }

    //LOG_VERBOSE("Wake-up time: %llums", LPTIMER_TICKS_TO_MS(lptimer_now_corrected());
}

void comm_task_sleep_until_hs(uint64_t time)
{
    uint64_t now = hs_timer_get_current_timestamp();

    if (time > (now + SLEEP_THRESHOLD_HS) ) {
        hs_timer_timeout_start(time, comm_task_unblock_callback);
        comm_task_block();
    } else if (time > now) {
        // LOG_VERBOSE("Insufficient time to use hstimer (start required in %lluus)", HS_TIMER_TICKS_TO_US(time - now));
        delay_us(HS_TIMER_TICKS_TO_US(time - now));
    } else {
        LOG_WARNING("Sleep until has been called %4lluus too late (expected start time was %llums)", HS_TIMER_TICKS_TO_US(now - time), HS_TIMER_TICKS_TO_MS(time));
    }
}

inline void comm_task_end_sleep()
{
    // De-activate timer
    lptimer_set(0, 0);

    // Unblock task - simulate firing of timer
    comm_task_unblock_callback();
}

inline void comm_task_block()
{
    ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
}

/* Unblocks the comm task */
inline void comm_task_unblock_callback(void)
{
    if (IS_INTERRUPT()) {
        vTaskNotifyGiveFromISR(xTaskHandle_comm, NULL);
    } else {
        xTaskNotifyGive(xTaskHandle_comm);
    }
}

void comm_task_update_dc()
{
    // Print phase information
    rtos_print_phase_dc();

#if LOG_ENABLE && (LOG_LEVEL > LOG_LEVEL_INFO)
    // Print the duty cycle information of the last round
    uint32_t rx_dc      = PPM_TO_HUNDREDS_OF_PERC(radio_get_rx_dc());
    uint32_t tx_dc      = PPM_TO_HUNDREDS_OF_PERC(radio_get_tx_dc());
    uint32_t cpu_dc     = rtos_get_cpu_dc();
    uint32_t cpu_log_dc = PPM_TO_HUNDREDS_OF_PERC(dcstat_get_dc(&log_cpu_dc));
    LOG_VERBOSE("DC - RX: %u.%02u%%; TX: %u.%02u%%; CPU Total: %u.%02u%%; CPU Log: %u.%02u%%", rx_dc / 100, rx_dc % 100, tx_dc / 100, tx_dc % 100, cpu_dc / 100, cpu_dc % 100, cpu_log_dc / 100, cpu_log_dc % 100);
#endif /* LOG_LEVEL > LOG_LEVEL_INFO */

    // Reset the duty cycle counters
    rtos_reset_cpu_dc();
    dcstat_reset(&log_cpu_dc);
    radio_dc_counter_reset();
}
