/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * States
 *
 * This file provides the helper functions used to change the network state.
 *
 */

#include "main.h"

/* Static functions ----------------------------------------------------------*/

static uint8_t states_init_radio(const comm_state_t* state);


/* Functions -----------------------------------------------------------------*/

void enter_bootstrapping(comm_state_t* state, bool reset_radio)
{
    // Reset radio
    if (reset_radio) {
        LOG_INFO("No node left in the network -> performing a reset\n--------------------------");
        log_flush();

        // Reset the radio
        radio_init();
    }

#if !LOG_PRINT_IMMEDIATELY
    // Always flush the log to prevent build-up during bootstrapping (log DC is not affected during bootstrapping)
    uint64_t time_before = lptimer_now_corrected();
    if (!log_flush()) {
        LOG_ERROR("Failed to flush log in %llums", LPTIMER_TICKS_TO_MS(lptimer_now_corrected() - time_before));
    }
#endif /* !LOG_PRINT_IMMEDIATELY */

    // Generate random clock offset
    uint64_t clk_ofs = NR_SLOTS_SL * LPTIMER_TICKS_TO_HS_TIMER(SLOT_DURATION_LP) + (rand() % BS_CLOCK_OFFSET_MAX_HS);
    hs_timer_set_counter(clk_ofs);
    hs_timer_set_drift(1);
    hs_timer_set_offset(0);
    update_clock_offset();
    set_drift_compensation_reference(true);

    // Reset the radio DC counters so it will not be counted for the SL slot
    rtos_add_dc_to_phase(state->next_slot.type);

    // Reset the comm_state - only variables with a potential to be initialized to unequal 0 are set individually afterwards
    memset(state, 0, sizeof(comm_state_t));
    request_update(state);
    state->next_slot.type = SLOT_TYPE_SLEEP;
    SET_FLAG(state->member_flags_e, NODE_ID);

    // Enter the BOOTSTRAPPING state
    state->network_state = STATE_BOOTSTRAPPING;
    STATE_BOOTSTRAPPING_IND();

    // Enable RxBoosted
    radio_set_rx_gain(true);

    // Adapt radio parameters
    uint8_t band = states_init_radio(state);

    // Craft a special schedule to listen for and transmit YD packets
    memset(state->schedule_curr, SCHEDULE_DISTRIBUTION_ID, NR_SLOTS_DD);  // Force all nodes to listen
    if (band == FRQ_BAND_BOOTSTRAPPING) {
        state->schedule_curr[NR_SLOTS_DD - 1] = NODE_ID; // Send YD packet
    }

    LOG_INFO("Bootstrapping on Ch %2hhu with offset %10llu", band, clk_ofs);
}

void enter_joining(comm_state_t* state)
{
    // Clear the schedule
    state->sched_vers = 0;
    memset(state->schedule_curr, 0, NR_SLOTS_DD);
    memset(state->schedule_mask, 0, NR_SLOTS_DD);

    // Enter the JOINING state
    state->network_state = STATE_JOINING;
    STATE_JOINING_IND();
    STATE_RECEIVING_IND();

    // Adapt radio parameters
    states_init_radio(state);

    if (state->synced) {
        LOG_INFO("Joining with clock at %llums", HS_TIMER_TICKS_TO_MS(hs_timer_get_current_timestamp()));
    } else {
        LOG_WARNING("Joining without being synchronized");
    }
}

void enter_normal(comm_state_t* state)
{
    // Enter the NORMAL state
    state->network_state = STATE_NORMAL;
    STATE_NORMAL_IND();

    // Adapt radio parameters
    states_init_radio(state);

    LOG_INFO("Joined  with clock at %llums", HS_TIMER_TICKS_TO_MS(hs_timer_get_current_timestamp()));
}

static uint8_t states_init_radio(const comm_state_t* state)
{
    // Set the correct TX power
    int8_t tx_power = states_get_tx_power(state);
    gloria_set_tx_power(tx_power);
    chaos_set_tx_power(tx_power);

    // Set the correct frequency band
    uint8_t band = states_get_freq_band(state);
    gloria_set_band(band);
    chaos_set_band(band);

    return band;
}

int8_t states_get_tx_power(const comm_state_t* state)
{
    switch (state->network_state) {
        case STATE_BOOTSTRAPPING:
            return TX_POWER_BOOTSTRAPPING;
        case STATE_JOINING:
        case STATE_NORMAL:
            return USED_RADIO_TX_POWER;
        default:
            LOG_ERROR("Unknown state: %u", state->network_state);
            return TX_POWER_NORMAL;
    }
}

uint8_t states_get_freq_band(const comm_state_t* state)
{
    switch (state->network_state) {
        case STATE_BOOTSTRAPPING:
#ifdef DROP_NODE_PACKETS_ENABLE
            // Excluded node should always directly bootstrap on main channel
            /*if DROP_NODE_PACKETS_FROM_ID(NODE_ID) {
                return USED_RADIO_FRQ_BAND;
            }*/
#endif /* DROP_NODE_PACKETS_ENABLE */
#ifdef BS_PROBABILITY_BOOT_CH
            if ( (rand() % 100) < BS_PROBABILITY_BOOT_CH) {
                return FRQ_BAND_BOOTSTRAPPING;
            } else {
                return USED_RADIO_FRQ_BAND;
            }
#else
            return FRQ_BAND_BOOTSTRAPPING;
#endif /* BS_PROBABILITY_BOOT_CH */
        case STATE_JOINING:
        case STATE_NORMAL:
            return USED_RADIO_FRQ_BAND;
        default:
            LOG_ERROR("Unknown state: %u", state->network_state);
            return FRQ_BAND_NORMAL;
    }
}
