/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Schedule
 *
 * This file contains functions related to creating and using the schedule.
 *
 */

#include "main.h"


/* Functions -----------------------------------------------------------------*/

slot_info_t get_next_used_slot_information(schedule_t* schedule, const schedule_mask_t* schedule_mask, uint64_t* starting_time)
{
    // The slot number of the next complete slot (with increasing slot numbers across rounds)
    uint64_t global_slot_number = (*starting_time + SLOT_DURATION_LP - 1UL) / SLOT_DURATION_LP;
    // The number of the current round
    uint64_t round_number       = global_slot_number / SLOTS_PER_ROUND;
    // The number of the slot in the current round
    uint64_t local_slot_number  = global_slot_number % SLOTS_PER_ROUND;

    *starting_time = global_slot_number * SLOT_DURATION_LP;

    if (local_slot_number == (SLOTS_PER_ROUND - NR_SLOTS_SD) ) {
        // The current slot is a Schedule distribution slot
        return (slot_info_t) {SLOT_TYPE_SCHEDULE_DISTRIBUTION, 0, 0};

    } else if (local_slot_number == (SLOTS_PER_ROUND - NR_SLOTS_SD - NR_SLOTS_SN) ) {
        // The first slot is the first one of a block of Schedule negotiation slots
        return (slot_info_t) {SLOT_TYPE_SCHEDULE_NEGOTIATION, 0, 0};

    } else if (local_slot_number > (SLOTS_PER_ROUND - NR_SLOTS_SD - NR_SLOTS_SN) ) {
        // The current slot would be a Schedule negotiation slot, the node would however start in the middle of the block of Schedule negotiation slots.
        // To prevent the timing issues this would create, the node skips the Schedule negotiation and directly waits for the Schedule distribution slot
        *starting_time += (SLOTS_PER_ROUND - NR_SLOTS_SD - local_slot_number) * SLOT_DURATION_LP;
        return (slot_info_t) {SLOT_TYPE_SCHEDULE_DISTRIBUTION, 0, 0};

    } else if ( (local_slot_number >= NR_SLOTS_SL)               &&
                (schedule[local_slot_number - NR_SLOTS_SL] != 0) &&
                !schedule_mask[(local_slot_number - NR_SLOTS_SL)]  ) {
        // The next slot is a used Data dissemination slot

        // first_data_slot is set to 0 since we might not have time to start early
        return (slot_info_t) {SLOT_TYPE_DATA_DISSEMINATION, schedule[local_slot_number - NR_SLOTS_SL], 0};

    } else {
        // The next slot is a Sleep slot or an unassigned Data dissemination slot
        // --> Determine the next used slot

        // Determine the next used Data dissemination or Schedule negotiation slot:

        // If the current slot is a Sleep slot, start looking for the next used Data dissemination slot at the beginning of the schedule,
        // otherwise start with the offset of the current Data dissemination slot.
        uint32_t first_slot = 0;
        if (local_slot_number >= NR_SLOTS_SL) {
            first_slot = local_slot_number - NR_SLOTS_SL;
        }

        // If the rest of the schedule is empty, the resulting offset will be the offset of the first Schedule negotiation slot
        uint32_t offset_in_schedule = NR_SLOTS_DD;
        // Iterate over the schedule to find the next assigned Data dissemination slot, if there is one
        for (uint32_t i = first_slot; i < NR_SLOTS_DD; i++) {
            if ( (schedule[i] != 0) && !schedule_mask[i]) {
                offset_in_schedule = i;
                break;
            }
        }

        // The next slot used slot is either a Data dissemination or Schedule negotiation slot

        // Set the starttime according to the computed offset of the slot in the schedule
        *starting_time = (round_number * SLOTS_PER_ROUND + NR_SLOTS_SL + offset_in_schedule) * SLOT_DURATION_LP;

        if (offset_in_schedule < NR_SLOTS_DD) {
            // The next used slot is a Data dissemination slot
            return (slot_info_t) {SLOT_TYPE_DATA_DISSEMINATION, schedule[offset_in_schedule], local_slot_number <= NR_SLOTS_SL};
        } else {
            // The next used slot is a Schedule negotiation slot
            return (slot_info_t) {SLOT_TYPE_SCHEDULE_NEGOTIATION, 0, 0};
        }
    }
}

/* Updates the schedule if SN was successful and the schedule is up-to-date.
 * For every node: If the node requested a reduction in the number of slots by k, then the first k slots assigned to that node are freed.
 * If a node requested additional k slots, then the last free slots are assigned to that node (First node 1 gets new slots, then 2,...). */
bool schedule_compute(comm_state_t* state, int8_t* requests)
{
    if (state->updated) {
        // Node is waiting to distribute new schedule
        LOG_VERBOSE("Skipping schedule update as already prepared vers %hhu", state->sched_vers + 1);
        return false;
    }

    // Initialize next schedule with current one
    memcpy(state->schedule_next, state->schedule_curr, NR_SLOTS_DD);

    bool all_added = false;
    bool changed   = false;

#if SN_LOG_REQUEST_VALUES
    for (node_id_t i = 0; i < MAX_NR_NODES; i++) {
        LOG_VERBOSE("Rq %hhu -> %hhi", i, requests[i]);
    }
#endif /* SN_LOG_REQUEST_VALUES */

#if SCHEDULE_ENABLE_DIRECT_TRANSFER
    // If slots can be directly transferred from one node to another, we must prevent their use locally as soon as the request has been sent

    // Free the slots as requested by the nodes - start from the front (slot 0)
    for (uint32_t slot = 0; slot < NR_SLOTS_DD; slot++) {
        node_id_t nd_id = state->schedule_next[slot];
        if ( (nd_id > 0) && (requests[nd_id - 1] < 0) ) {
            state->schedule_next[slot] = 0;
            changed = true;

            // Only decrease the number of slots still needed if the value does not correspond to SLOT_COUNT_NEUTRAL_ELEMENT (as such a node is not in the network anymore)
            if (requests[nd_id - 1] != SLOT_COUNT_NEUTRAL_ELEMENT) {
                requests[nd_id - 1]++;
            }
        }
    }
#endif /* SCHEDULE_ENABLE_DIRECT_TRANSFER */

    // Assign the free slots as requested - start from the end (slot NR_SLOTS_DD - 1)
    node_id_t last_id = 0;
    for (int32_t slot = NR_SLOTS_DD - 1; (slot >= 0) && !all_added; slot--) {
        // If the slot is not used anymore: reassign it
        if (state->schedule_next[slot] == 0) {
            bool found = false;

            // Iterate over all node IDs - fulfill requests from the lowest node ID first
            for (node_id_t nd_id_new = last_id; (nd_id_new < MAX_NR_NODES) && !found; nd_id_new++) {
                // If the node still needs more slots and it is considered a part of the network:
                // Assign the current slot to it
                if ( (requests[nd_id_new] > 0)                        &&
                     IS_FLAG_SET(state->member_flags_r, nd_id_new + 1)  ) {
                    // Update schedule
                    state->schedule_next[slot] = nd_id_new + 1;
                    requests[nd_id_new]--;
                    changed = true;

                    // Update variables used to improve performance of schedule computation
                    found   = true;
                    last_id = nd_id_new;
                }
            }

            // If no node was found: All nodes have enough slots --> No need to continue
            if (!found) {
                all_added = true;
            }
        }
        else if (slot == 0) {
            LOG_WARNING("Some requests could not be fulfilled, as no free slots are remaining!");
        }
    }

#if !SCHEDULE_ENABLE_DIRECT_TRANSFER
    // Free the slots as requested by the nodes - start from the front (slot 0)
    for (uint32_t slot = 0; slot < NR_SLOTS_DD; slot++) {
        node_id_t nd_id = state->schedule_next[slot];
        if ( (nd_id > 0) && (requests[nd_id - 1] < 0) ) {
            state->schedule_next[slot] = 0;
            changed = true;

            // Only decrease the number of slots still needed if the value does not correspond to SLOT_COUNT_NEUTRAL_ELEMENT (as such a node is not in the network anymore)
            if (requests[nd_id - 1] != SLOT_COUNT_NEUTRAL_ELEMENT) {
                requests[nd_id - 1]++;
            }
        }
    }
#endif /* !SCHEDULE_ENABLE_DIRECT_TRANSFER */

    state->updated = changed;

    return state->updated;
}

#if SD_SCHEDULE_COMPRESS
bool schedule_pack(const schedule_t* schedule, schedule_distribution_packet_t* packet)
{
    memset(packet->schedule, 0, SD_PACKET_NR_SCHEDULE_BYTES);

    uint32_t ofs  = 0;
    uint32_t slot = 0;
    for (uint32_t i = 0; i < SD_PACKET_NR_SCHEDULE_BYTES; i++) {
        while (ofs < 8) {
            uint16_t  curr_slot  = (schedule[slot] & ( (1 << SD_SCHEDULE_COMPRESSION_BITS) - 1) ) << ofs;
            packet->schedule[i] |= curr_slot & 0xFF;
            if (curr_slot & 0xFF00) {
                // Fill lower part of next byte
                packet->schedule[i + 1] |= curr_slot >> 8;
            }

            ofs += SD_SCHEDULE_COMPRESSION_BITS;
            slot++;
        }

        // Prepare the offset for the next byte
        ofs %= 8;
    }

    return true;
}
#endif /* SD_SCHEDULE_COMPRESS */

bool schedule_update(comm_state_t* state, schedule_distribution_packet_t* packet)
{
    // Copy schedule from packet
    state->sched_vers = packet->sched_vers;
#if SD_SCHEDULE_COMPRESS
    uint32_t ofs = 0;
    for (uint32_t i = 0; i < NR_SLOTS_DD; i++) {
        state->schedule_curr[i] = (packet->schedule[ofs / 8] >> (ofs % 8) ) & ( (1 << SD_SCHEDULE_COMPRESSION_BITS) - 1);
        if ( ( (ofs % 8) + SD_SCHEDULE_COMPRESSION_BITS) > 8) {
            // Fetch lower part of next byte
            uint32_t       next_ofs  = (ofs % 8) + SD_SCHEDULE_COMPRESSION_BITS - 8;
            state->schedule_curr[i] += (packet->schedule[(ofs / 8) + 1] & ( (1 << next_ofs) - 1) ) << (SD_SCHEDULE_COMPRESSION_BITS - next_ofs);
        }

        ofs += SD_SCHEDULE_COMPRESSION_BITS;
    }

    // Check that compression led to correct result by comparing the de-compressed schedule with the original
    /*if (state->updated && (memcmp(state->schedule_curr, state->schedule_next, NR_SLOTS_DD) != 0) ) {
        LOG_ERROR("Compression of schedule corrupted result for sched vers %hhu", state->sched_vers);
    }*/
#else
    memcpy(state->schedule_curr, packet->schedule, NR_SLOTS_DD);
#endif /* SD_SCHEDULE_COMPRESS */

    // Reset the schedule mask
    memset(state->schedule_mask, 0, NR_SLOTS_DD);

    // Update request for next epoch
    request_update(state);

    // Reset state
    state->updated = false;

#if FL_TRACE_SCHEDULING
    // Set indication if transmitting
    if (schedule_get_nr_slots(state, NODE_ID) > 0) {
        STATE_TRANSMITTING_IND();
    } else {
        STATE_RECEIVING_IND();
    }
#endif /* FL_TRACE_SCHEDULING */

    return true;
}

/* Returns true if the schedule is empty.
 *   schedule: Schedule to check
 */
bool is_schedule_empty(const schedule_t* schedule)
{
    // Slots are filled from the back, so we start checking from the back
    for (int32_t i = NR_SLOTS_DD - 1; i >= 0; i--) {
        if (schedule[i] != 0) {
            return false;
        }
    }
    return true;
}

uint8_t schedule_get_nr_slots(comm_state_t* state, node_id_t node_id)
{
    uint8_t nr_slots = 0;

    for (uint32_t i = 0; i < NR_SLOTS_DD; i++) {
        if (state->schedule_curr[i] == node_id) {
            nr_slots++;
        }
    }

    return nr_slots;
}

slot_count_t request_update(comm_state_t* state)
{
    // Choose a random value in [RQ_NR_DATA_SLOTS_MIN, RQ_NR_DATA_SLOTS_MAX]
    state->desired_nr_slots = RQ_NR_DATA_SLOTS_MIN + ( rand() % (RQ_NR_DATA_SLOTS_MAX - RQ_NR_DATA_SLOTS_MIN + 1) );
    LOG_VERBOSE("Currently desired slots: %hhu", state->desired_nr_slots);

#if SCHEDULE_ENABLE_DIRECT_TRANSFER
    // In the case that the node requested to reduce its number of assigned slots:
    // Do not use these slots in case that they were re-allocated
    int8_t change = (int8_t)(state->desired_nr_slots - schedule_get_nr_slots(state, NODE_ID));
    if (change < 0) {
        // Mark slots as unusable to prevent collisions
        for (uint8_t i = 0; (i < NR_SLOTS_DD) && (change < 0); i++) {
            if (state->schedule[i] == NODE_ID) {
                state->schedule_mask[i] = true;
                change++;
            }
        }

        LOG_VERBOSE("Released %hhi previously owned slots locally", -change);
    }
#endif /* SCHEDULE_ENABLE_DIRECT_TRANSFER */

    return state->desired_nr_slots;
}
