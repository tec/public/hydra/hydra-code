/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Slots
 *
 * This file contains some helper functions used for analysing received packets and member flags.
 *
 */

#include "main.h"


/* Functions -----------------------------------------------------------------*/

void run_end_of_round(comm_state_t* state)
{
    // Update epoch offset
    if (state->network_state != STATE_BOOTSTRAPPING) {
        state->epoch_offset++;
    }

    // Handle End of Epoch
    if (state->epoch_offset == NR_ROUNDS_EPOCH) {

        state->updated      = false;
        state->unchanged    = false;
        state->epoch_offset = 0;

        // Determine whether node is part of a valid network and schedule can be kept
        uint32_t sum_I_e = 0;
        for (uint32_t i = 0; i < MAX_NR_NODES; i++) {
            sum_I_e += state->I_e[i];
        }

        if (sum_I_e <= MAX_NR_NODES/2) {
            LOG_INFO("Insufficient nodes in network (%2hhu) to maintain schedule %2hhu", sum_I_e, state->sched_vers);
            STATE_RECEIVING_IND();

            state->sched_vers = 0;
            memset(state->schedule_curr, 0, NR_SLOTS_DD);

            // Increase counter for unsynchronized epochs
            state->E++;
            if (state->E >= NR_EPOCHS_TO_LEAVE) {
                state->synced = false;
                enter_bootstrapping(state, false);
            }
        } else {
            state->E = 0;
        }

        // Update epoch member flags
        for (uint32_t i = 0; i < MAX_NR_NODES; i++) {
            if ( ( !IS_FLAG_SET(state->member_flags_e, i + 1) && (state->C_e[i] >= NR_ROUNDS_TO_JOIN) ) ||
                 (  IS_FLAG_SET(state->member_flags_e, i + 1) && (state->C_e[i] >= NR_ROUNDS_TO_STAY) ) ||
                 ( (i + 1) == NODE_ID)                                                                    ) {
                SET_FLAG(state->member_flags_e, i + 1);
            } else {
                CLEAR_FLAG(state->member_flags_e, i + 1);
            }
        }

        // Update the counters
        memset(state->C_e, 0, MAX_NR_NODES);
        memset(state->I_e, 0, MAX_NR_NODES);
    }

    // FreeRTOS house-keeping

    // Update the duty cycle counters
    rtos_add_dc_to_phase(state->next_slot.type);
    comm_task_update_dc();

    // Check whether stack usage is not excessive
    if (!rtos_check_stack_usage()) {
        LOG_ERROR("Stack usage exceeded limits");
    }
}

uint64_t extract_timestamp(const uint8_t* packet, const uint8_t size)
{
    uint64_t timestamp; // This timestamp should have equal length to CHAOS_TIMESTAMP_LENGTH and GLORIA_TIMESTAMP_LENGTH
    uint8_t* timestamp_ptr;

    switch (packet[0] & PROTOCOL_ID_MASK) {
        case PROTOCOL_ID_CHAOS:
            if (!( ((chaos_header_t*)packet)->sync) || size != (CHAOS_HEADER_LENGTH_WITH_TS + sizeof(schedule_negotiation_packet_t) ) ) {
                // There is no timestamp in the Chaos packet --> ignore
                return 0;
            }

            timestamp_ptr = ((chaos_header_t*)packet)->timestamp;
            break;
        case PROTOCOL_ID_GLORIA:
            // Check the size and whether there is a timestamp in the packet
            if (!( ((gloria_header_t*)packet)->sync) || size < (GLORIA_HEADER_LENGTH_MIN + GLORIA_TIMESTAMP_LENGTH + DD_HEADER_LENGTH) ) {
                // There is no timestamp in the Gloria packet --> ignore
                return 0;
            }

            timestamp_ptr = ((gloria_header_t*)packet)->min.timestamp;
            break;
        default:
            LOG_ERROR("Unknown protocol encountered");
            return 0;
    }

    memcpy((uint8_t*)&timestamp, timestamp_ptr, sizeof(uint64_t));
    return timestamp;
}

bool get_packet_time_offset(const uint8_t* packet, const uint8_t size, int64_t* time_offset_hs)
{
    // Give up if the packet is too small
    if (size < sizeof(uint64_t)) {
        return 0;
    }

    // The time offset to add to the final timestamp
    int64_t rx_offset_hs = gloria_timings[GLORIA_INTERFACE_MODULATION].txSync;

    switch (packet[0] & PROTOCOL_ID_MASK) {
        case PROTOCOL_ID_CHAOS:
            LOG_VERBOSE("Chaos msg");
            if (!( ((chaos_header_t*)packet)->sync) || size != (CHAOS_HEADER_LENGTH_WITH_TS + sizeof(schedule_negotiation_packet_t) ) ) {
                // There is no timestamp in the Chaos packet --> ignore
                return 0;
            }
            break;
        case PROTOCOL_ID_GLORIA:
            LOG_VERBOSE("Gloria msg");
            // Check the size and whether there is a timestamp in the packet
            if (!( ((gloria_header_t*)packet)->sync) || size < (GLORIA_HEADER_LENGTH_MIN + GLORIA_TIMESTAMP_LENGTH + DD_HEADER_LENGTH) ) {
                // There is no timestamp in the Gloria packet --> ignore
                return 0;
            }

            rx_offset_hs += ((gloria_header_t*)packet)->slot_index * gloria_calculate_slot_time(GLORIA_INTERFACE_MODULATION, 0, 0, size) + gloria_timings[GLORIA_INTERFACE_MODULATION].floodInitOverhead;
            break;
        default:
            LOG_ERROR("Unknown protocol encountered");
            return 0;
    }

    // Extract the timestamp included in the packet
    uint64_t packet_timestamp_hs = extract_timestamp(packet, size);
    if (packet_timestamp_hs == 0) {
        LOG_WARNING("Failed to extract timestamp");
        return 0;
    } else {
        LOG_VERBOSE("Rcvd pkt timestamp: %llums", HS_TIMER_TICKS_TO_MS(packet_timestamp_hs));
    }

    // Set return values
    *time_offset_hs = (int64_t)(packet_timestamp_hs - radio_get_last_sync_timestamp()) + rx_offset_hs;
    return 1;
}

bool get_member_flags_offset(const uint8_t* packet, const uint8_t size, const uint8_t* member_flags_offset)
{
    switch (packet[0] & PROTOCOL_ID_MASK) {
        case PROTOCOL_ID_CHAOS:
            LOG_VERBOSE("Chaos msg");
            if (size != (sizeof(chaos_header_t) + sizeof(schedule_negotiation_packet_t)) ) {
                return 0;
            }

            uint8_t chaos_hdr_len = (((chaos_header_t *)packet)->sync) ? CHAOS_HEADER_LENGTH_WITH_TS : CHAOS_HEADER_LENGTH;
            member_flags_offset   = ((schedule_negotiation_packet_t*)(packet + chaos_hdr_len))->member_flags;
            break;
        case PROTOCOL_ID_GLORIA:
            LOG_VERBOSE("Gloria msg");
            if (size < (GLORIA_HEADER_LENGTH_MIN + DD_HEADER_LENGTH)) {
                return 0;
            }

            // Note: This assumes that the offset is equivalent for both DD and SD packets
            uint8_t gloria_hdr_len = (((gloria_header_t*)packet)->sync) ? (GLORIA_HEADER_LENGTH_MIN + GLORIA_TIMESTAMP_LENGTH) : GLORIA_HEADER_LENGTH_MIN;
            member_flags_offset    = ((data_dissemination_packet_t*)(packet + gloria_hdr_len))->member_flags;
            break;
        default:
            LOG_ERROR("Unknown protocol encountered");
            return 0;
    }
    return 1;
}

inline uint8_t get_nr_of_contributors(const uint8_t* flags) {
    uint8_t nr_nodes = 0;
    for (node_id_t node = 0; node < MAX_NR_NODES; node++) {
        if (IS_FLAG_SET(flags, node + 1)) {
            nr_nodes++;
        }
    }

    return nr_nodes;
}

bool is_dominant_network(const uint8_t* member_flags_rcvd,const uint8_t* member_flags_local)
{
    // Compute the number of nodes in both networks and the smallest node ID in the networks
    uint32_t  nr_nodes_rcvd    = 0;
    node_id_t first_node_rcvd  = 0;
    uint32_t  nr_nodes_local   = 0;
    node_id_t first_node_local = 0;

    // Count the number of nodes in each network
    for (uint32_t i = 0; i < NR_MEMBER_FLAG_BYTES; i++) {
        for (uint32_t j = 0; j < 8; j++) {
            if (member_flags_rcvd[i] & (1 << j) ) {
                nr_nodes_rcvd++;
                first_node_rcvd = (first_node_rcvd == 0) ? (8 * i + j) : first_node_rcvd;
            }
            if (member_flags_local[i] & (1 << j) ) {
                nr_nodes_local++;
                first_node_local = (first_node_local == 0) ? (8 * i + j) : first_node_local;
            }
        }
    }

    // If any of the networks has more nodes: That network should be merged with
    if (nr_nodes_rcvd > nr_nodes_local) {
        return true;
    }
    if (nr_nodes_rcvd < nr_nodes_local) {
        return false;
    }

    // If both networks have the same size, then the one which contains the node with the highest ID should be merged with
    return (first_node_rcvd > first_node_local);
}

bool is_member_flag_empty(const uint8_t* member_flag)
{
    // Iterate over all member flag bytes and check whether it is empty
    for (uint32_t i = 0; i < NR_MEMBER_FLAG_BYTES; i++) {
        if (member_flag[i]) {
            return false;
        }
    }
    return true;
}
