/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Slot: Data dissemination
 *
 * This code implements the Data dissemination slot, which is used to send and receive application data.
 *
 */

#include "main.h"


/* Static variables ----------------------------------------------------------*/
static comm_state_t*               comm_state;
static data_dissemination_packet_t dd_packet;
static uint8_t                     gloria_band = 0;


/* Static functions ----------------------------------------------------------*/

static inline uint8_t gloria_get_band(comm_state_t* state)
{
    // The Gloria interface does not provide this functionality, so we need to gather the information through the Chaos interface
    if (state->network_state == STATE_BOOTSTRAPPING) {
        // During bootstrapping, the Chaos band is equivalent to the Gloria band
        return chaos_get_band();
    } else {
        return USED_RADIO_FRQ_BAND;
    }
}


/* Functions -----------------------------------------------------------------*/

/* Filters packets from other networks. */
bool data_dissemination_filter_fn(const uint8_t* gloria_hdr, uint8_t hdr_len, const uint8_t* payload, uint8_t size)
{
    data_dissemination_packet_t* pkt = (data_dissemination_packet_t*)payload;

#if DROP_NODE_PACKETS_ENABLE
    if (DROP_NODE_PACKETS_PIN_VALUE && DROP_NODE_PACKETS_FROM_ID(pkt->id_src) && ( (rand() % 100) < DROP_NODE_PACKETS_PROBABILITY)) {
        return false;
    }
#endif /* DROP_NODE_PACKETS_ENABLE */

#if CHANGE_TX_POWER_ENABLE
    if (states_get_tx_power(comm_state) == RADIO_MIN_POWER) {
        return false;
    }
#endif /* CHANGE_TX_POWER_ENABLE */

    if (size < DD_HEADER_LENGTH) {
        LOG_WARNING("Rcvd pkt has wrong len (%hhuB)", size);
        return false;
    }

    // Permit all packets in bootstrapping mode
    if (comm_state->network_state == STATE_BOOTSTRAPPING) {
        // Trigger timer so reception is immediately processed
        comm_task_end_sleep();

        return true;
    }

#if FILTER_PACKETS_FROM_WRONG_NETWORK
    // Check if the message comes from the correct network
    if (!IS_FLAG_SET(pkt->member_flags, NODE_ID)) {
        LOG_INFO("Wrong source network");
        return false;
    }
#endif /* FILTER_PACKETS_FROM_WRONG_NETWORK */

    // Check if the sender is correct
    if        (pkt->id_src == SCHEDULE_DISTRIBUTION_ID) {
        LOG_ERROR("Wrong type (rcvd schedule but expected data from %hhu)", comm_state->next_slot.sender);
        return false;
#if FILTER_PACKETS_FROM_WRONG_SENDER
    } else if (pkt->id_src != comm_state->next_slot.sender) {
        LOG_ERROR("Wrong sender (rcvd from %hhu but expected %hhu)", pkt->id_src, comm_state->next_slot.sender);
        return false;
#endif /* FILTER_PACKETS_FROM_WRONG_SENDER */
    }

    // Check whether the timestamp of the packet is from the current slot
    uint64_t pkt_timestamp = extract_timestamp(gloria_hdr, hdr_len + size);
    if (pkt_timestamp == 0) {
        LOG_WARNING("Failed to extract timestamp from pkt from node %hhu", pkt->id_src);
        return false;
    } else if ( (pkt_timestamp - LPTIMER_TICKS_TO_HS_TIMER(comm_state->next_slot_start_time - GLORIA_GUARD_TIME_LONG_LP) ) > LPTIMER_TICKS_TO_HS_TIMER(SLOT_DURATION_LP + GLORIA_GUARD_TIME_LONG_LP)) {
        LOG_WARNING("Wrong timestamp from node %hhu", pkt->id_src);
        return false;
    }

    return true;
}

void run_data_dissemination_slot(comm_state_t* state)
{
    // Store the pointer to the comm_state such that the packet filter function has access to it
    comm_state = state;

#if CHANGE_TX_POWER_ENABLE
    // Set the TX power
    int8_t gloria_tx_power = states_get_tx_power(state);
    gloria_set_tx_power(gloria_tx_power);

    // Adjust RxBoosted
    radio_set_rx_gain(gloria_tx_power > RADIO_MIN_POWER);
#endif /* CHANGE_TX_POWER_ENABLE */

    gloria_band = gloria_get_band(state);
#if CHANGE_FRQ_BAND_ENABLE
    // Set the frequency band
    gloria_set_band(gloria_band);
#endif /* CHANGE_FRQ_BAND_ENABLE */

    if (state->next_slot.sender == NODE_ID) {
        // Broadcast data

        // Prepare the packet
        dd_packet.id_src = NODE_ID;
        memcpy(dd_packet.member_flags, state->member_flags_e, NR_MEMBER_FLAG_BYTES);
        memset(dd_packet.payload,      NODE_ID,               DD_PAYLOAD_LENGTH);

        // Start initiating a Gloria flood
        gloria_start(true,
                     (uint8_t*)&dd_packet,
                     DD_HEADER_LENGTH + DD_PAYLOAD_LENGTH,
                     GLORIA_MAX_RETRANSMISSIONS,
                     1);
        comm_task_sleep_lp(GLORIA_MAX_DURATION_LP);
        gloria_stop();
        LOG_VERBOSE("Pkt sent");

    } else {
        // Listen for data

        // Start listening for Gloria floods
        gloria_set_pkt_filter(data_dissemination_filter_fn);
        gloria_start(false,
                     (uint8_t*)&dd_packet,
                     DD_HEADER_LENGTH + DD_PAYLOAD_LENGTH,
                     GLORIA_MAX_RETRANSMISSIONS,
                     1);

        // Wait until the end of the slot
        uint64_t listen_time_lp = GLORIA_MAX_DURATION_LP + GLORIA_GUARD_TIME_LP + (state->next_slot.first_data_slot ? GLORIA_GUARD_TIME_LONG_LP : 0);

        if (state->network_state == STATE_BOOTSTRAPPING) {
            // Skip all other slots until the last slot of the DD phase
            uint8_t next_slot_number = (lptimer_now_corrected() + MAX_SCHEDULE_LOOKUP_TIME_LP + SLOT_DURATION_LP - 1UL) / SLOT_DURATION_LP;
            uint8_t slots_to_wait    = NR_SLOTS_SL + NR_SLOTS_DD - 1 - (next_slot_number % SLOTS_PER_ROUND);

            listen_time_lp += (uint64_t)slots_to_wait * SLOT_DURATION_LP;
            LOG_VERBOSE("Listening for up to %hhu slots", slots_to_wait + 1);
        }

        comm_task_sleep_lp(listen_time_lp);

        // Process the packet if one was received
        if (gloria_stop()) {
            LOG_VERBOSE("Rcvd   pkt from %2hhu of payload len %2hhuB in %hhu hops", dd_packet.id_src, gloria_get_payload_len() - DD_HEADER_LENGTH, gloria_get_rx_index() + 1);

            // Correct the clock offset
            uint64_t timestamp_rx;
            if (gloria_get_received_timestamp((uint8_t*)&timestamp_rx)) {
                //LOG_VERBOSE("Successfully synced to node %i", state->next_slot.sender);
            } else {
                LOG_WARNING("Failed to sync      to node %i", state->next_slot.sender);
            }

            // Adjust HS timer
            double delta;
            if (gloria_get_t_ref_hs() < timestamp_rx) {
                delta =   (double)(timestamp_rx          - gloria_get_t_ref_hs());
            } else {
                delta = - (double)(gloria_get_t_ref_hs() - timestamp_rx);
            }
            hs_timer_adapt_offset(delta);
            if        (ABS(delta) >  SYNC_THRESHOLD_GLORIA_WARN_HS) {
                LOG_WARNING("HS offset adjusted by %llius", HS_TIMER_TICKS_TO_US((int64_t)delta));
            } else if (ABS(delta) >= SYNC_THRESHOLD_GLORIA_INFO_HS) {
                LOG_VERBOSE("HS offset adjusted by %llius", HS_TIMER_TICKS_TO_US((int64_t)delta));
            }

            // Sync LP to HS timer
            update_clock_offset();

            switch (state->network_state) {
                case STATE_BOOTSTRAPPING:
                    // Switch state if existing network has been detected
                    if (gloria_band != FRQ_BAND_BOOTSTRAPPING) {
                        state->synced = true;

                        // Update the epoch offset based on the synchronized time, as the existing network has already progressed
                        state->epoch_offset = (lptimer_now_corrected() / SLOT_DURATION_LP / SLOTS_PER_ROUND) % NR_ROUNDS_EPOCH;

                        enter_joining(state);
                    }
                    break;
                default:
                    break;
            }
        } else {

            switch (state->network_state) {
                case STATE_BOOTSTRAPPING:
                    // Reset if listened on normal channel
                    if (gloria_band != FRQ_BAND_BOOTSTRAPPING) {
                        enter_bootstrapping(state, false);
                    }
                    break;
                case STATE_JOINING:
                case STATE_NORMAL:
                    LOG_VERBOSE("Missed pkt from %2hhu", state->next_slot.sender);
                    break;
                default:
                    break;
            }
        }

        /* Higher-layer application could process the data here using dd_packet->payload */
    }
}
