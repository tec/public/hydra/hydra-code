/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Slot: Schedule negotiation
 *
 * This code implements the Schedule negotiation slot, in which the changes for the next schedule are aggregated.
 * This slot is also used for requesting to join a network or for agreeing to merge with some other network.
 *
 */

#include "main.h"


/* Static variables ----------------------------------------------------------*/
static comm_state_t*                 comm_state;
static schedule_negotiation_packet_t sn_packet;
static uint16_t                      pkt_rcvd_cnt;
static uint16_t                      pkt_merged_cnt;
static uint16_t                      filter_applied_cnt;
static uint16_t                      filter_passed_cnt;


/* Functions -----------------------------------------------------------------*/

/* Returns the request value of a specific node stored in an SN packet.
 *   pkt:     Pointer to the SN packet to look into.
 *   node_id: ID of the node of which the request value is returned.
 */
static int8_t get_request_value(schedule_negotiation_packet_t* pkt, node_id_t node_id)
{
    // The offset of the target value in bits
    uint32_t offset = (node_id - 1) * SN_NR_DELTA_BITS;
    // Extract the byte containing the bits of the current value
    uint16_t val    = pkt->requests[offset / 8];

    // If some bits of the current request value are in the next byte: Append that byte to the current temporary value
    if ( ( (offset % 8) + SN_NR_DELTA_BITS) > 8) {
        val += ( (uint16_t)pkt->requests[ (offset / 8) + 1] ) << 8;
    }

    // Shift the bits of the target ID to remove the offset
    int8_t target_value  = (int8_t)(val >> (offset % 8) );
    // Extract only bits of the target ID
           target_value &= (1 << SN_NR_DELTA_BITS) - 1;
    // Add the minimum value to obtain the negative values
    return target_value + SLOT_COUNT_NEUTRAL_ELEMENT;
}

/* Extracts all the request values and writes the result into an array.
 *   pkt:    SN packet from which the request values are taken.
 *   result: Pointer to an array of size MAX_NR_NODES. The result is written to this array.
 */
static void get_request_values(schedule_negotiation_packet_t* pkt, int8_t* result)
{
    uint32_t  offset  = 0;
    node_id_t node_id = 0;

    for (uint32_t i = 0; i < SN_PACKET_NR_REQUEST_BYTES; i++) {
        uint16_t curr_byte = pkt->requests[i];

        while ( (offset < 8) && (node_id < MAX_NR_NODES) ) {
            // If some bits of the current request value are in the next byte, append that byte to the current temporary value
            if ( (offset + SN_NR_DELTA_BITS) > 8) {
                curr_byte += ( (uint16_t)pkt->requests[i + 1] ) << 8;
            }

            // Shift the bits of the current request value to remove the offset
            int8_t target_value  = (int8_t)(curr_byte >> offset);
            // Extract only bits of the target ID
                   target_value &= (1 << SN_NR_DELTA_BITS) - 1;
            // Add the minimal value to obtain the negative values
            result[node_id]      = target_value + SLOT_COUNT_NEUTRAL_ELEMENT;

            // Increase the counters
            node_id++;
            offset += SN_NR_DELTA_BITS;
        }

        // Prepare the offset for the next byte
        offset %= 8;
    }
}

/* Sets the request value of a specific node stored in an SN packet if it is higher than the previous value.
 *   pkt:       SN packet to look into.
 *   node_id:   Node ID of which the request value is to be set.
 *   new_value: New request value in the interval [SLOT_COUNT_DELTA_MIN, SLOT_COUNT_DELTA_MAX].
 *
 *   Return value: True if new information was received
 */
static bool set_request_value(schedule_negotiation_packet_t* pkt, node_id_t node_id, int8_t new_value)
{
    // The offset of the target value in bits
    uint32_t offset = (node_id - 1) * SN_NR_DELTA_BITS;
    // Copy the byte containing the request value to a temporary value
    uint16_t val    = pkt->requests[offset / 8];
    // If some bits of the request value are contained in the next byte: Append this byte to the temporary value
    if ( ( (offset % 8) + SN_NR_DELTA_BITS) > 8) {
        val += ( (uint16_t)pkt->requests[offset / 8 + 1] ) << 8;
    }

    // The mask, in which only the bits of the request value of the node are set
    uint16_t mask            = ( (1 << SN_NR_DELTA_BITS) - 1) << (offset % 8);

    // Extract the previous value
    uint16_t conv_prev_value = val & mask;

    // Add -SLOT_COUNT_NEUTRAL_ELEMENT to the new value in order to obtain a non-negative integer. The result is shifted to the right position
    uint16_t conv_new_value  = ( (uint16_t) ((int16_t)new_value - SLOT_COUNT_NEUTRAL_ELEMENT) ) << (offset % 8);

    // Only update the value if the value has not been set or is new
    if ( (conv_prev_value == conv_new_value) || (new_value == SLOT_COUNT_NEUTRAL_ELEMENT) ) {
        // No action required, as previous value will be retained
        return false;
    } else if (conv_prev_value == 0) {
        // Clear the bits corresponding to the request value of the node
        val &= ~mask;

        // Set the bits corresponding to the request value of the node
        val += conv_new_value;

        // Write the temporary value back into the array
        pkt->requests[offset / 8] = val & 0x00FF;
        // If some bits of the value are contained in the next byte: Write that one back as well
        if ( ( (offset % 8) + SN_NR_DELTA_BITS) > 8) {
            pkt->requests[ (offset / 8) + 1] = val >> 8;
        }

        return true;
    } else {
        LOG_ERROR("Attempt to change request for node %2hhu from %3u to %3u", node_id, conv_prev_value, conv_new_value);
        return false;
    }
}

/* Efficient merging implementation leveraging that SLOT_COUNT_NEUTRAL_ELEMENT is represented through 0. */
static bool merge_request_values(schedule_negotiation_packet_t* pkt, schedule_negotiation_packet_t* pkt_rx)
{
    bool changed = false;
    uint32_t ofs = 0;
    for (uint32_t i = 0; i < SN_PACKET_NR_REQUEST_BYTES; i++) {
        // Combine previous and received request
        uint16_t prev_request = pkt->requests[i];
        uint16_t rx_request   = pkt_rx->requests[i];
        if (i < (SN_PACKET_NR_REQUEST_BYTES - 1)) {
            prev_request += (pkt->requests[i+1]    << 8);
            rx_request   += (pkt_rx->requests[i+1] << 8);
        }

        // Generate mask for existing requests as well as relevant received information
        uint16_t prev_request_mask = 0;
        uint16_t rx_request_mask   = 0;
        while (ofs < 8) {
            uint16_t request_mask  = ((1 << SN_NR_DELTA_BITS) - 1) << ofs;
            prev_request_mask     += request_mask * ( (prev_request & request_mask) > 0);
            rx_request_mask       += request_mask * ( (rx_request   & request_mask) > 0);
            ofs                   += SN_NR_DELTA_BITS;
        }

        // Reduce received information to the scope of the current request byte
        rx_request &= rx_request_mask;

        // Sanity check: No different existing information is permitted
        if ( (rx_request & prev_request_mask) ^ (prev_request & rx_request_mask) ) {
            LOG_WARNING("Attempt to change request starting at index %hhu from %u to %u", i, prev_request, rx_request & prev_request_mask);
            return false;
        }

        // Check if new information is contained
        rx_request &= ~prev_request_mask;
        if (rx_request > 0) {
            // Store the new requests back into the current packet
            pkt->requests[i] |= rx_request & 0x00FF;
            if (rx_request & 0xFF00) {
                pkt->requests[i+1] |= rx_request >> 8;
            }

            changed = true;
        }

        // Prepare the offset for the next byte
        ofs %= 8;
    }

    return changed;
}

/* Sets the request value of the node with NODE_ID stored in an SN packet.
 * The other request values are set to SLOT_COUNT_NEUTRAL_ELEMENT.
 *  pkt:       Pointer to the SN packet to look into.
 *  new_value: New request value in the interval [SLOT_COUNT_DELTA_MIN, SLOT_COUNT_DELTA_MAX].
 */
static void init_request_values(schedule_negotiation_packet_t* pkt, int8_t new_value)
{
    // Sets all stored requests to the converted version of SLOT_COUNT_NEUTRAL_ELEMENT
    memset(pkt->requests, 0, SN_PACKET_NR_REQUEST_BYTES);

    set_request_value(pkt, NODE_ID, new_value);
}

/* Determines the earlier schedule version. The calculation is modified such that counter overflows are not an issue:
 * [Z0, Z1 - 1] is considered earlier than [Z1, Z2 - 1].
 * [Z1, Z2 - 1] is considered earlier than [Z2, Z0 - 1].
 * [Z2, Z0 - 1] is considered earlier than [Z0, Z1 - 1].
 * Inside these intervals, the ordering is determined using the normal MIN function.
 * Zero is considered the neutral element of the operation.
 *   ctr1: Schedule version 1
 *   ctr2: Schedule version 2
 *
 *   Return value: True if ctr2 is strictly earlier.
 */
static bool schedule_negotiation_determine_min_sched_vers(uint8_t ctr1, uint8_t ctr2)
{
    // Check for equality
    if (ctr1 == ctr2) {
        return false;
    }

    // Check whether any of the values is zero (neutral element)
    if        (ctr1 == 0) {
        return true;
    } else if (ctr2 == 0) {
        return false;
    }

    // The normal comparison
    if (ctr1 < ctr2) {
        // Handle the overflow
        if ( (ctr2 >= SN_COMPARISON_START_ZONE_2) && (ctr1 < SN_COMPARISON_START_ZONE_1) ) {
            return true;
        } else {
            return false;
        }
    } else {
        // Handle the overflow
        if ( (ctr1 >= SN_COMPARISON_START_ZONE_2) && (ctr2 < SN_COMPARISON_START_ZONE_1) ) {
            return false;
        } else {
            return true;
        }
    }
}

/* Determines the later schedule version. The calculation is modified such that counter overflows are not an issue:
 * [Z0, Z1 - 1] is considered earlier than [Z1, Z2 - 1].
 * [Z1, Z2 - 1] is considered earlier than [Z2, Z0 - 1].
 * [Z2, Z0 - 1] is considered earlier than [Z0, Z1 - 1].
 * Inside these intervals, the ordering is determined using the normal MAX function.
 * Zero is considered the neutral element of the operation.
 *   ctr1: Schedule version 1
 *   ctr2: Schedule version 2
 *
 *   Return value: True if ctr2 is strictly later.
 *
 *   Note: This is not the inverse of "schedule_negotiation_compute_counter_min", as equality and neutral elements are treated differently.
 */
static bool schedule_negotiation_determine_max_sched_vers(uint8_t ctr1, uint8_t ctr2)
{
    // Check for equality
    if (ctr1 == ctr2) {
        return false;
    }

    // Check whether any of the values is zero (neutral element)
    if        (ctr1 == 0) {
        return true;
    } else if (ctr2 == 0) {
        return false;
    }

    // The normal comparison
    if (ctr1 < ctr2) {
        // Handle the overflow
        if ( (ctr2 >= SN_COMPARISON_START_ZONE_2) && (ctr1 < SN_COMPARISON_START_ZONE_1) ) {
            return false;
        } else {
            return true;
        }
    } else {
        // Handle the overflow
        if ( (ctr1 >= SN_COMPARISON_START_ZONE_2) && (ctr2 < SN_COMPARISON_START_ZONE_1) ) {
            return true;
        } else {
            return false;
        }
    }
}

/* The function used to filter SN packets */
static bool schedule_negotiation_message_filter(const uint8_t* payload_rx, const uint8_t size_rx, const uint8_t* payload, const uint8_t size)
{
    uint8_t hdr_len                    = (((chaos_header_t*)payload_rx)->sync) ? CHAOS_HEADER_LENGTH_WITH_TS : CHAOS_HEADER_LENGTH;
    schedule_negotiation_packet_t* pkt = (schedule_negotiation_packet_t*)(payload_rx + hdr_len);

#if DROP_NODE_PACKETS_ENABLE
    if (DROP_NODE_PACKETS_PIN_VALUE && DROP_NODE_PACKETS_FROM_ID(pkt->id_src) && ( (rand() % 100) < DROP_NODE_PACKETS_PROBABILITY) ) {
        return false;
    }
#endif /* DROP_NODE_PACKETS_ENABLE */

#if DROP_SCHEDULE_NEGOTIATION_ENABLE
    if (DROP_SCHEDULE_NEGOTIATION_PIN_VALUE && ( (rand() % 100) < DROP_SCHEDULE_NEGOTIATION_PROBABILITY) ) {
        return false;
    }
#endif /* DROP_SCHEDULE_NEGOTIATION_ENABLE */

#if CHANGE_TX_POWER_ENABLE
    if (states_get_tx_power(comm_state) == RADIO_MIN_POWER) {
        return false;
    }
#endif /* CHANGE_TX_POWER_ENABLE */
    filter_applied_cnt++;

    // Check packet size and whether the packet contains a timestamp
    if (size_rx != (CHAOS_HEADER_LENGTH_WITH_TS + sizeof(schedule_negotiation_packet_t)) ||
        !((chaos_header_t*)payload_rx)->sync) {
        LOG_WARNING("Rcvd pkt has wrong len (%hhuB)", size_rx);
        return false;
    }
    if (size != sizeof(schedule_negotiation_packet_t)) {
        LOG_ERROR("Local pkt has wrong len (%hhuB)", size);
        return false;
    }

#if SN_ADD_PAYLOAD_CRC
    uint32_t crc32_rx = crc32((uint8_t*)pkt, sizeof(schedule_negotiation_packet_t) - sizeof(uint32_t), 0);
    if (crc32_rx != pkt->crc32) {
        LOG_WARNING("Incorrect payload CRC from node %hhu", pkt->id_src);
        return false;
    }
#endif /* SN_ADD_PAYLOAD_CRC */

#if SN_LOG_PACKET_CRC
    LOG_VERBOSE("CRC Filter: %lu", crc32((uint8_t*)pkt, sizeof(schedule_negotiation_packet_t), 1));
#endif /* SN_LOG_PACKET_CRC */

    // Check whether the timestamp of the packet is from the current slot
    uint64_t pkt_timestamp = extract_timestamp(payload_rx, size_rx);
    if (pkt_timestamp == 0) {
        LOG_WARNING("Failed to extract timestamp from pkt from node %hhu", pkt->id_src);
        return false;
    } else if ( (pkt_timestamp - LPTIMER_TICKS_TO_HS_TIMER(comm_state->next_slot_start_time) ) > LPTIMER_TICKS_TO_HS_TIMER(SLOT_DURATION_LP * NR_SLOTS_SN) ) {
        LOG_WARNING("Wrong timestamp from node %hhu", pkt->id_src);
        return false;
    }

    // Packet passed the filter
    filter_passed_cnt++;
    return true;
}

#if SKIP_SCHEDULE_NEGOTIATION_ENABLE && FLOCKLAB
/* This message filter is used to simulate an unsuccessful SN slot. */
bool schedule_negotiation_message_filter_ignore_all(uint8_t* payload_rx, uint8_t size_rx, uint8_t* payload, uint8_t size)
{
    filter_applied_cnt++;

    return false;
}
#endif /* SKIP_SCHEDULE_NEGOTIATION_ENABLE */

/* Merges SN packets. */
static bool schedule_negotiation_merge_fn(uint8_t* payload, uint8_t size, const uint8_t* payload_rx, const uint8_t size_rx)
{
    pkt_rcvd_cnt++;

    if (size_rx != sizeof(schedule_negotiation_packet_t)) {
        LOG_ERROR("Rcvd pkt has wrong len (%hhuB)", size_rx);
        return false;
    }
    if (size != sizeof(schedule_negotiation_packet_t)) {
        LOG_ERROR("Local pkt has wrong len (%hhuB)", size);
        return false;
    }

    schedule_negotiation_packet_t* pkt    = (schedule_negotiation_packet_t*)payload;
    schedule_negotiation_packet_t* pkt_rx = (schedule_negotiation_packet_t*)payload_rx;
    bool changed = false;

    // Set C_r
    for (node_id_t i = 0; i < MAX_NR_NODES; i++) {
        if (get_request_value(pkt_rx, i + 1) != SLOT_COUNT_NEUTRAL_ELEMENT) {
            comm_state->C_r[i] = 1;
        }
    }

    // Check whether packet should be merged
    if ( (IS_FLAG_SET(pkt->member_flags, pkt_rx->id_src) && IS_FLAG_SET(pkt_rx->member_flags, pkt->id_src) ) ||
         (comm_state->network_state == STATE_BOOTSTRAPPING)                                                    ) {
        pkt_merged_cnt++;

        // Set I_e
        for (node_id_t i = 0; i < MAX_NR_NODES; i++) {
            if (get_request_value(pkt_rx, i + 1) != SLOT_COUNT_NEUTRAL_ELEMENT) {
                comm_state->I_e[i] = 1;
            }
        }

        // Merge the minimum and maximum schedule version counter
        if (schedule_negotiation_determine_min_sched_vers(pkt->sched_vers_min, pkt_rx->sched_vers_min)) {
            pkt->sched_vers_min = pkt_rx->sched_vers_min;
        }
        if (schedule_negotiation_determine_max_sched_vers(pkt->sched_vers_max, pkt_rx->sched_vers_max)) {
            pkt->sched_vers_max = pkt_rx->sched_vers_max;
        }

        // Merge the member flags
        for (uint32_t i = 0; i < NR_MEMBER_FLAG_BYTES; i++) {
            changed              |= (pkt_rx->member_flags[i] & ~pkt->member_flags[i]) > 0;
            pkt->member_flags[i] |=  pkt_rx->member_flags[i];
        }

        // Merge the request values
        changed |= merge_request_values(pkt, pkt_rx);
        /* Legacy: Unoptimized variant (~25us less efficient with default network settings)
        for (node_id_t i = 0; i < MAX_NR_NODES; i++) {
            changed |= set_request_value(pkt, i + 1, get_request_value(pkt_rx, i + 1));
        }*/

        // Merge the min and max drift values
        pkt->drift_min = MIN(pkt->drift_min, pkt_rx->drift_min);
        pkt->drift_max = MAX(pkt->drift_max, pkt_rx->drift_max);
    }

#if SN_ADD_PAYLOAD_CRC
    // Update CRC
    pkt->crc32 = crc32((uint8_t*)pkt, sizeof(schedule_negotiation_packet_t) - sizeof(uint32_t), 0);
#endif /* SN_ADD_PAYLOAD_CRC */

    switch (comm_state->network_state) {
        case STATE_JOINING:
            // Enforce that joining nodes transmit aggressively so that they are included by an existing network
            return (changed || ( (rand() % 100) < BS_PROBABILITY_CHAOS_TX) );
        default:
            break;
    }

    return changed;
}

/* Determines whether an SN packet is complete. */
static chaos_termination_result_t schedule_negotiation_termination_fn(const uint8_t* payload, const uint8_t size)
{
    // Check packet size
    if (size != sizeof(schedule_negotiation_packet_t)) {
        LOG_ERROR("Local pkt has wrong len (%hhuB)", size);
        return CHAOS_PACKET_NOT_COMPLETE;
    }

    // Check if all the nodes in the network participated
    schedule_negotiation_packet_t* pkt = (schedule_negotiation_packet_t*)payload;
    bool flags_empty                   = true;
    for (node_id_t i = 0; i < MAX_NR_NODES; i++) {
        if (IS_FLAG_SET(pkt->member_flags, i + 1)) {
            flags_empty = false;
            if (get_request_value(pkt, i + 1) == SLOT_COUNT_NEUTRAL_ELEMENT) {
                return CHAOS_PACKET_NOT_COMPLETE;
            }
        }
    }

    if (flags_empty) {
        return CHAOS_PACKET_NOT_COMPLETE;
    }

    switch (comm_state->network_state) {
        case STATE_BOOTSTRAPPING:
            // Member flags initially contain only the node itself and are hence by definition complete -> enforce sharing
            return CHAOS_PACKET_NOT_COMPLETE;
        case STATE_JOINING:
            return CHAOS_PACKET_COMPLETE_BUT_KEEP_TRANSMITTING;
        default:
            break;
    }

#if SN_LOG_PACKET_CRC
    LOG_VERBOSE("CRC Terminated: %lu", crc32(payload, size, 1));
#endif /* SN_LOG_PACKET_CRC */

    return CHAOS_PACKET_COMPLETE_BUT_KEEP_LISTENING;
}

void run_schedule_negotiation_slot(comm_state_t* state)
{
    comm_state         = state;
    pkt_rcvd_cnt       = 0;
    pkt_merged_cnt     = 0;
    filter_applied_cnt = 0;
    filter_passed_cnt  = 0;
    memset(&sn_packet, 0, sizeof(schedule_negotiation_packet_t));

    double                clock_offset_delta   = get_accumulated_hstimer_correction();
    uint64_t              measurement_duration = get_drift_measurement_duration_hs();
    double                computed_drift       = compute_new_drift(clock_offset_delta, measurement_duration);
    chaos_initiate_type_t initiate_slot        = (state->network_state == STATE_JOINING) ? CHAOS_INITIATE_WITH_OTHERS_AFTER_RX_TIMEOUT : CHAOS_INITIATE_WITH_OTHERS;
    int8_t                curr_request         = 0;
    uint8_t               curr_nr_slots        = schedule_get_nr_slots(state, NODE_ID);

    // Initialize round counters
    memset(state->C_r, 0, MAX_NR_NODES);

    // Initialize the packet
    sn_packet.id_src         = NODE_ID;
    sn_packet.status         = 0;
    sn_packet.sched_vers_min = state->sched_vers;
    sn_packet.sched_vers_max = state->sched_vers;
    sn_packet.drift_min      = (int16_t)(computed_drift * SN_PACKET_DRIFT_SCALE);
    sn_packet.drift_max      = (int16_t)(computed_drift * SN_PACKET_DRIFT_SCALE);
    memcpy(sn_packet.member_flags, state->member_flags_e, NR_MEMBER_FLAG_BYTES);

    // Set requested change; prevent an overflow if the desired change is not within [SLOT_COUNT_DELTA_MIN, SLOT_COUNT_DELTA_MAX].
    if (state->desired_nr_slots > curr_nr_slots) {
        curr_request = MIN(SLOT_COUNT_DELTA_MAX,         state->desired_nr_slots - curr_nr_slots);
    } else {
        curr_request = MAX(SLOT_COUNT_DELTA_MIN, (int8_t)state->desired_nr_slots - curr_nr_slots);
    }
    init_request_values(&sn_packet, curr_request);

#if SN_ADD_PAYLOAD_CRC
    sn_packet.crc32 = crc32((uint8_t*)&sn_packet, sizeof(schedule_negotiation_packet_t) - sizeof(uint32_t), 0);
#endif /* SN_ADD_PAYLOAD_CRC */

    if (curr_request) {
        LOG_INFO("Requested change: %2hhi", curr_request);
    }

#if CHANGE_TX_POWER_ENABLE
    // Set the TX power
    int8_t chaos_tx_power = states_get_tx_power(state);
    chaos_set_tx_power(chaos_tx_power);

    // Adjust RxBoosted
    radio_set_rx_gain(chaos_tx_power > RADIO_MIN_POWER);
#endif /* CHANGE_TX_POWER_ENABLE */

#if CHANGE_FRQ_BAND_ENABLE
    // Set the frequency band
    chaos_set_band(states_get_freq_band(state));
#endif /* CHANGE_FRQ_BAND_ENABLE */

#if SKIP_SCHEDULE_NEGOTIATION_ENABLE && FLOCKLAB
    if (SKIP_SCHEDULE_NEGOTIATION_PIN_VALUE) {
        // If the SN slot should be skipped:
        // In order for the node to still be able to update the schedule and the timeout counters as if the node would have communication issues, SN is not actually skipped.
        // Instead, an SN slot is performed in which the node is not the initiator and in which the node filters out all packets.
        chaos_start((uint8_t*)&sn_packet,
                    sizeof(sn_packet),
                    schedule_negotiation_merge_fn,
                    schedule_negotiation_termination_fn,
                    schedule_negotiation_message_filter_ignore_all,
                    CHAOS_DO_NOT_INITIATE,
                    LPTIMER_TICKS_TO_HS_TIMER(state->next_slot_start_time)                         + CHAOS_SLOT_START_OFFSET_HS,
                    LPTIMER_TICKS_TO_HS_TIMER(state->next_slot_start_time + CHAOS_DURATION_MAX_LP) + CHAOS_SLOT_START_OFFSET_HS,
                    true,
                    CHAOS_TIMESYNC_FULL,
                    true);
    } else {
#else
    {
#endif /* SKIP_SCHEDULE_NEGOTIATION_ENABLE */
        chaos_start((uint8_t*)&sn_packet,
                    sizeof(schedule_negotiation_packet_t),
                    schedule_negotiation_merge_fn,
                    schedule_negotiation_termination_fn,
                    schedule_negotiation_message_filter,
                    initiate_slot,
                    LPTIMER_TICKS_TO_HS_TIMER(state->next_slot_start_time)                         + CHAOS_SLOT_START_OFFSET_HS,
                    LPTIMER_TICKS_TO_HS_TIMER(state->next_slot_start_time + CHAOS_DURATION_MAX_LP) + CHAOS_SLOT_START_OFFSET_HS,
                    true,
                    CHAOS_TIMESYNC_FULL,
                    true);
    }

    comm_task_sleep_lp(CHAOS_DURATION_MAX_LP);

    // Process the result of SN
    if (chaos_stop()) {
        LOG_INFO("SN successful");

        // Store member flags for use in SD phase and EOR
        memcpy(state->member_flags_r, sn_packet.member_flags, NR_MEMBER_FLAG_BYTES);

        uint8_t nr_nodes_in_network = get_nr_of_contributors((const uint8_t*)sn_packet.member_flags);
        if (nr_nodes_in_network > MAX_NR_NODES/2) {

            if (sn_packet.sched_vers_min == sn_packet.sched_vers_max) {
                if (state->sched_vers > 0) {
                    // Extract the request values from the SN packet
                    int8_t requests[MAX_NR_NODES];
                    get_request_values(&sn_packet, requests);

                    // Update schedule
                    if (schedule_compute(state, requests)) {
                        LOG_INFO("Schedule updated to vers %hhu", state->sched_vers + 1);
                    } else {
                        state->unchanged = true;
                        LOG_VERBOSE("Schedule remained at vers %hhu", state->sched_vers);
                    }
                } else if (sn_packet.sched_vers_min == 0) {
                    // All nodes in the network had to reject their schedule, so we need to bootstrap again
                    state->synced = false;
                }
            } else if (state->sched_vers == sn_packet.sched_vers_max) {
                state->retransmit = true;
                LOG_VERBOSE("Schedule %hhu will be re-transmitted", state->sched_vers);
            } else {
                LOG_INFO("Schedule %hhu not up-to-date", state->sched_vers);
            }
        } else {
            LOG_WARNING("Network has insufficient size %hhu", nr_nodes_in_network);
        }

        if (state->synced || !is_schedule_empty(state->schedule_curr) ) {
            // Update the drift compensation factor such that the drift compensation does not get higher and higher in the network
            update_drift_compensation(computed_drift,
                                      ( (double)sn_packet.drift_min / SN_PACKET_DRIFT_SCALE),
                                      ( (double)sn_packet.drift_max / SN_PACKET_DRIFT_SCALE));
        } else {
            // If the node did not yet receive the schedule, we do not update the drift compensation factor
            set_drift_compensation_reference(false);
        }

        // Update the network state
        switch (state->network_state) {
            case STATE_BOOTSTRAPPING:
                LOG_ERROR("Encountered existing network in SN phase");
                state->synced = true;
                enter_joining(state);
                break;
            case STATE_JOINING:
                // Detect if the join request was successful and adjust the state accordingly
                if (IS_FLAG_SET(sn_packet.member_flags, NODE_ID)) {
                    enter_normal(state);
                }
                break;
            case STATE_NORMAL:
                // If no valid schedule is available in the network anymore, the network needs to disband and re-start
                if (!state->synced) {
                    enter_bootstrapping(state, false);
                }
            default:
                break;
        }

    } else {
        LOG_WARNING("SN not successful");

        // Prepare for the next drift compensation update
        set_drift_compensation_reference(false);

        // Update the network state
        switch (state->network_state) {
            case STATE_BOOTSTRAPPING: {
                // Check whether majority could sync
                uint8_t nr_discovered_nodes = get_nr_of_contributors((const uint8_t*)sn_packet.member_flags);
                if (nr_discovered_nodes > MAX_NR_NODES/2) {
                    // Start new network based on the discovered nodes
                    state->synced = true;
                    memcpy(state->member_flags_e, sn_packet.member_flags, NR_MEMBER_FLAG_BYTES);

                    // Prepare to distribute sync
                    state->retransmit = true;
                    memcpy(state->member_flags_r, sn_packet.member_flags, NR_MEMBER_FLAG_BYTES);

                    LOG_VERBOSE("Discovered %2hhu nodes -> distributing sync", nr_discovered_nodes - 1);
                } else {
                    LOG_VERBOSE("Discovered %2hhu nodes -> insufficient to sync", nr_discovered_nodes - 1);
                }

                // Check whether an existing network has been discovered
                if (sn_packet.sched_vers_min > 0) {
                    LOG_ERROR("Encountered existing network in SN phase");
                    state->synced = true;
                    enter_joining(state);
                }
                break;
            }
            default:
                break;
        }
    }

    // Set C_e
    for (node_id_t i = 0; i < MAX_NR_NODES; i++) {
        state->C_e[i] += state->C_r[i];
    }

    // Correct next slot start time, since SN uses more than one slot (if NR_SLOTS_SN > 1)
    state->next_slot_start_time += (NR_SLOTS_SN - 1) * SLOT_DURATION_LP;

    // Update the offset of the lptimer in case the hstimer was corrected during the SN protocol
    update_clock_offset();

    // Prevent division by zero when printing debug output
    uint16_t tot_applied = filter_applied_cnt;
    uint16_t tot_rcvd    = pkt_rcvd_cnt;
    if (tot_applied == 0) { tot_applied = 1; }
    if (tot_rcvd    == 0) { tot_rcvd    = 1; }
    LOG_VERBOSE("Msg filtering: %u (%u%%) out of %u pkts passed", filter_passed_cnt, filter_passed_cnt * 100 / tot_applied, filter_applied_cnt);
    LOG_VERBOSE("Msg merging:   %u (%u%%) out of %u pkts merged", pkt_merged_cnt,    pkt_merged_cnt    * 100 / tot_rcvd,    pkt_rcvd_cnt);
    if (pkt_rcvd_cnt > filter_applied_cnt) {
        LOG_WARNING("Only applied filtering to %u (%u%%) out of %u pkts; discarded %u invalid pkts", filter_applied_cnt, filter_applied_cnt * 100 / tot_rcvd, pkt_rcvd_cnt, pkt_rcvd_cnt - filter_applied_cnt);
    }

    uint8_t participation_flags[NR_MEMBER_FLAG_BYTES] = {0};
    for (node_id_t i = 0; i < MAX_NR_NODES; i++) {
            participation_flags[i / 8] |= state->C_r[i] << (i % 8);
    }

    LOG_VERBOSE("Schedule versions: %hhu - %hhu", sn_packet.sched_vers_min, sn_packet.sched_vers_max);
    PRINT_FLAGS("Known network  ", state->member_flags_e);
    PRINT_FLAGS("SN pkt members ", sn_packet.member_flags);
    PRINT_FLAGS("SN pkt requests", participation_flags);
    LOG_VERBOSE("State after SN: %hhu", state->network_state);
}
