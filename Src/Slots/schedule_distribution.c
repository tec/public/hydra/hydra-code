/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Slot: Schedule distribution
 *
 * This code implements the Schedule distribution slot, in which the nodes broadcast or receive the schedule.
 *
 */

#include "main.h"


/* Static variables ----------------------------------------------------------*/
static comm_state_t*                  comm_state;
static schedule_distribution_packet_t sd_packet;


/* Functions -----------------------------------------------------------------*/

/* Filters packets other than Schedule distribution packets. */
bool schedule_distribution_filter_fn(const uint8_t* gloria_hdr, uint8_t hdr_len, const uint8_t* payload, uint8_t size)
{
#if DROP_NODE_PACKETS_ENABLE
    // Attention: As all packets are sent from the broadcast ID, message filtering according to ID must handle this case separately if desired
    if (DROP_NODE_PACKETS_PIN_VALUE && DROP_NODE_PACKETS_FROM_ID(SCHEDULE_DISTRIBUTION_ID) && ( (rand() % 100) < DROP_NODE_PACKETS_PROBABILITY)) {
        return false;
    }
#endif /* DROP_NODE_PACKETS_ENABLE */

#if CHANGE_TX_POWER_ENABLE
    if (states_get_tx_power(comm_state) == RADIO_MIN_POWER) {
        return false;
    }
#endif /* CHANGE_TX_POWER_ENABLE */

    // Check packet size
    if (size != sizeof(schedule_distribution_packet_t)) {
        LOG_VERBOSE("Wrong size (%hhuB)", size);
        return false;
    }

    schedule_distribution_packet_t* pkt = (schedule_distribution_packet_t*)payload;
    // Check if the packet is a Schedule distribution packet
    if (pkt->id_src != SCHEDULE_DISTRIBUTION_ID) {
        LOG_VERBOSE("Wrong type (rcvd data from %hhu but expected schedule)", pkt->id_src);
        return false;
    }

    // Check whether the timestamp of the packet is from the current slot
    uint64_t pkt_timestamp = extract_timestamp(gloria_hdr, hdr_len + size);
    if (pkt_timestamp == 0) {
        LOG_WARNING("Failed to extract timestamp");
        return false;
    } else if (comm_state->synced && ( (pkt_timestamp - LPTIMER_TICKS_TO_HS_TIMER(comm_state->next_slot_start_time) ) > LPTIMER_TICKS_TO_HS_TIMER(SLOT_DURATION_LP) ) ) {
        LOG_VERBOSE("Wrong timestamp");
        return false;
    }

    return true;
}

void run_schedule_distribution_slot(comm_state_t* state)
{
    // Store the pointer to the comm_state such that the packet filter function has access to it
    comm_state = state;

#if CHANGE_TX_POWER_ENABLE
    // Set the TX power
    int8_t gloria_tx_power = states_get_tx_power(state);
    gloria_set_tx_power(gloria_tx_power);

    // Adjust RxBoosted
    radio_set_rx_gain(gloria_tx_power > RADIO_MIN_POWER);
#endif /* CHANGE_TX_POWER_ENABLE */

#if CHANGE_FRQ_BAND_ENABLE
    // Set the frequency band
    gloria_set_band(states_get_freq_band(state));
#endif /* CHANGE_FRQ_BAND_ENABLE */

    // Broadcast the schedule at the end of the epoch if it is updated
    if (state->updated) {
        if (state->epoch_offset == (NR_ROUNDS_EPOCH - 1)) {
            // Transmit schedule
            LOG_VERBOSE("Distributing new schedule %hhu", state->sched_vers + 1);

            // Initialise the Schedule distribution packet
            memcpy(sd_packet.member_flags, state->member_flags_r, NR_MEMBER_FLAG_BYTES);

            // Sender ID 0 is not used by any node --> SD packet will not be confused with a DD packet
            sd_packet.id_src = SCHEDULE_DISTRIBUTION_ID;
#if SD_SCHEDULE_COMPRESS
            schedule_pack(state->schedule_next, &sd_packet);
#else
            memcpy(sd_packet.schedule, state->schedule_next, NR_SLOTS_DD);
#endif /* SD_SCHEDULE_COMPRESS */

            // Handle roll-over of variable (do not set to neutral element)
            if (state->sched_vers >= UINT8_MAX) {
                sd_packet.sched_vers = 1;
            } else {
                sd_packet.sched_vers = state->sched_vers + 1;
            }

            // Update locally
            schedule_update(state, &sd_packet);

            // Set the start marker such that all nodes start the flood at the same time
            gloria_set_tx_marker(LPTIMER_TICKS_TO_HS_TIMER(state->next_slot_start_time + SD_INIT_TIME_LP));

            // Start the Gloria flood
            gloria_start(true,
                         (uint8_t*)&sd_packet,
                         sizeof(schedule_distribution_packet_t),
                         GLORIA_MAX_RETRANSMISSIONS,
                         1);

            comm_task_sleep_lp(GLORIA_MAX_DURATION_LP);
            gloria_stop();
        } else {
            // No schedule will be transmitted before the end of the epoch for this network, so sleep to reduce energy consumption
            LOG_VERBOSE("Distributing new schedule %hhu in %hhu rounds", state->sched_vers + 1, NR_ROUNDS_EPOCH - 1 - state->epoch_offset);
            comm_task_sleep_lp(GLORIA_MAX_DURATION_LP);
        }
    // Re-transmit schedule if not all nodes in the network have the same version
    } else if (state->retransmit) {
        // Transmit schedule
        LOG_VERBOSE("Re-transmitting schedule %hhu", state->sched_vers);

        // Initialise the Schedule distribution packet
        memcpy(sd_packet.member_flags, state->member_flags_r, NR_MEMBER_FLAG_BYTES);

        // Sender ID 0 is not used by any node --> SD packet will not be confused with a DD packet
        sd_packet.id_src     = SCHEDULE_DISTRIBUTION_ID;
        sd_packet.sched_vers = state->sched_vers;

#if SD_SCHEDULE_COMPRESS
        schedule_pack(state->schedule_curr, &sd_packet);
#else
        memcpy(sd_packet.schedule, state->schedule_curr, NR_SLOTS_DD);
#endif /* SD_SCHEDULE_COMPRESS */

        // Set the start marker such that all nodes start the flood at the same time
        gloria_set_tx_marker(LPTIMER_TICKS_TO_HS_TIMER(state->next_slot_start_time + SD_INIT_TIME_LP));

        // Start the Gloria flood
        gloria_start(true,
                     (uint8_t*)&sd_packet,
                     sizeof(schedule_distribution_packet_t),
                     GLORIA_MAX_RETRANSMISSIONS,
                     1);

        comm_task_sleep_lp(GLORIA_MAX_DURATION_LP);
        gloria_stop();

        // Do not automatically re-transmit multiple times, as schedules might be up-to-date everywhere next round
        state->retransmit = false;
    } else if (state->unchanged) {
        // No schedule will be transmitted before the end of the epoch for this network, so sleep to reduce energy consumption
        LOG_VERBOSE("Schedule %hhu remains for another %hhu rounds", state->sched_vers, NR_ROUNDS_EPOCH - state->epoch_offset);
        comm_task_sleep_lp(GLORIA_MAX_DURATION_LP);
    } else {
        // Receive schedule
        LOG_VERBOSE("Listening for schedule");

        gloria_set_pkt_filter(schedule_distribution_filter_fn);
        gloria_start(false,
                     (uint8_t*)&sd_packet,
                     sizeof(schedule_distribution_packet_t),
                     GLORIA_MAX_RETRANSMISSIONS,
                     1);

        comm_task_sleep_lp(GLORIA_MAX_DURATION_LP + SD_INIT_TIME_LP);

        // Update the schedule if one was received
        if (gloria_stop()) {
            LOG_INFO("Schedule %hhu rcvd", sd_packet.sched_vers);

            // Correct the clock offset
            uint64_t timestamp_rx;
            if (gloria_get_received_timestamp((uint8_t*)&timestamp_rx)) {
                //LOG_VERBOSE("Successfully synced to schedule");
            } else {
                LOG_WARNING("Failed to sync to schedule");
            }

            // Adjust HS timer
            double delta;
            if (gloria_get_t_ref_hs() < timestamp_rx) {
                delta =   (double)(timestamp_rx          - gloria_get_t_ref_hs());
            } else {
                delta = - (double)(gloria_get_t_ref_hs() - timestamp_rx);
            }
            hs_timer_adapt_offset(delta);
            if        (ABS(delta) >  SYNC_THRESHOLD_GLORIA_WARN_HS) {
                LOG_WARNING("HS offset adjusted by %llius", HS_TIMER_TICKS_TO_US((int64_t) delta));
            } else if (ABS(delta) >= SYNC_THRESHOLD_GLORIA_INFO_HS) {
                LOG_VERBOSE("HS offset adjusted by %llius", HS_TIMER_TICKS_TO_US((int64_t) delta));
            }

            // Sync LP to HS timer
            update_clock_offset();

            // Update schedule based on received packet
            schedule_update(state, &sd_packet);

            // Ensure that the schedule will still be used next round
            memset(state->I_e, 1, MAX_NR_NODES);

            switch (state->network_state) {
                case STATE_BOOTSTRAPPING:
                    // Join received sync signal
                    state->synced = true;
                    memcpy(state->member_flags_e, sd_packet.member_flags, NR_MEMBER_FLAG_BYTES);

                    LOG_INFO("Discovered %2hhu nodes -> received sync", get_nr_of_contributors((const uint8_t*)sd_packet.member_flags));
                    break;
                default:
                    break;
            }
        }
    }

    switch (state->network_state) {
        case STATE_BOOTSTRAPPING:
            // Switch channel and start normal scheduling behaviour
            if (state->synced) {
                enter_joining(state);

                // If a new network is started from scratch, the empty schedule must be regarded as valid
                state->sched_vers = 1;
            } else {
                // Reset state and re-start bootstrapping with different offset and channel
                enter_bootstrapping(state, false);
            }
            break;

        case STATE_JOINING:
            // Check if the join request was accepted
            if (!IS_FLAG_SET(sd_packet.member_flags, NODE_ID)) {
                LOG_INFO("Not included in schedule %hhu", state->sched_vers);
            } else {
                enter_normal(state);
            }
            break;

        default:
            break;
    }

    // Print information about the current schedule
    LOG_INFO("Schedule at end of round: %2hhu (hash: %10u)", state->sched_vers, crc32(state->schedule_curr, NR_SLOTS_DD, 0));
}
