/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Communication structures
 *
 * This file contains the definition of the important structs.
 * It also contains the declaration of some helper functions and of the state change and slot type indicator macros.
 *
 */

#ifndef __TASK_COMM_H
#define __TASK_COMM_H


/* --- definitions --- */

/* The length of one round in number of slots */
#define SLOTS_PER_ROUND           (NR_SLOTS_SL + NR_SLOTS_DD + NR_SLOTS_SN + NR_SLOTS_SD)

/* Number of bytes used for the member flags */
#define NR_MEMBER_FLAG_BYTES      ((MAX_NR_NODES + 7) / 8)

/* The sender ID DPP_DEVICE_ID_BROADCAST is used in Schedule distribution slots to distinguish Schedule distribution from Data dissemination packets. */
#define SCHEDULE_DISTRIBUTION_ID  ((uint8_t)DPP_DEVICE_ID_BROADCAST)

/* Determines whether a bit is set in the member flags. */
#define IS_FLAG_SET(flags, bit)   (((flags)[((bit) - 1) / 8] &   (1 << ( ((bit) - 1) % 8) ) ) > 0)

/* Sets a bit in the member flags. */
#define SET_FLAG(flags, bit)      ( (flags)[((bit) - 1) / 8] |=  (1 << ( ((bit) - 1) % 8) ) )

/* Clear a bit in the member flags. */
#define CLEAR_FLAG(flags, bit)    ( (flags)[((bit) - 1) / 8] &= ~(1 << ( ((bit) - 1) % 8) ) )

/* Determines whether the member flags are identical */
#define IS_SAME_NETWORK(flags1, flags2) (memcmp(flags1, flags2, NR_MEMBER_FLAG_BYTES) == 0)

/* --- typedefs --- */

/* The type of a node ID.
 * Note: The node ID 0 is reserved for "no node" / SCHEDULE_DISTRIBUTION_ID. */
typedef uint8_t node_id_t;

/* This type is used to store the number of assigned slots. */
typedef uint8_t slot_count_t;

/* The type for storing the schedule, which assigns a sender to every Data dissemination slot.
 * This must be instantiated as an array with NR_SLOTS_DD elements. */
typedef node_id_t schedule_t;

/* This type is used to store a mask specifying which slots should be ignored.
 * This must be instantiated as an array with NR_SLOTS_DD elements. */
typedef bool schedule_mask_t;

/* The different types of slots */
typedef enum {
    SLOT_TYPE_SLEEP,
    SLOT_TYPE_DATA_DISSEMINATION,
    SLOT_TYPE_SCHEDULE_NEGOTIATION,
    SLOT_TYPE_SCHEDULE_DISTRIBUTION,
    NR_SLOT_TYPES
} slot_type_t;

/* The states a network can be in */
typedef enum {
    STATE_BOOTSTRAPPING,
    STATE_JOINING,
    STATE_NORMAL,
} network_state_t;

/* Struct containing the information of a specific slot */
typedef struct {
    slot_type_t type;
    node_id_t   sender;
    bool        first_data_slot;
} slot_info_t;

/* The type for storing the state of the communication task */
typedef struct {
    // This variable stores the current network state
    network_state_t network_state;
    // This variable indicates the number of slots the node would like to have
    slot_count_t    desired_nr_slots;
    // This variable indicates the version number of the currently active schedule
    uint8_t         sched_vers;
    // A struct containing information about the next slot
    slot_info_t     next_slot;
    // The start time of the next slot
    uint64_t        next_slot_start_time;
    // The active schedule
    schedule_t      schedule_curr[NR_SLOTS_DD];
    // The schedule which will be distributed
    schedule_t      schedule_next[NR_SLOTS_DD];
    // Bool array indicating which slots should be skipped in the schedule
    schedule_mask_t schedule_mask[NR_SLOTS_DD];
    // Connectivity and interaction counters for every node. A node is removed from the network if it missed too many SN slots and must discard a schedule if it is not part of a network anymore.
    uint8_t         C_e[MAX_NR_NODES];
    uint8_t         C_r[MAX_NR_NODES];
    uint8_t         I_e[MAX_NR_NODES];
    // Bit flags representing the set of nodes currently considered part of the network
    // The member flags which will be used in the current epoch / round
    uint8_t         member_flags_e[NR_MEMBER_FLAG_BYTES];
    uint8_t         member_flags_r[NR_MEMBER_FLAG_BYTES];
    // Round state
    uint8_t         E;
    uint8_t         epoch_offset;
    bool            updated;
    bool            retransmit;
    bool            unchanged;
    bool            synced;
} comm_state_t;


/* --- function prototypes --- */

/* log_flush is only defined if LOG_PRINT_IMMEDIATELY is 0. It is however also needed when LOG_PRINT_IMMEDIATELY is 1.
 * This define allows the application to compile in both cases. */
#if LOG_PRINT_IMMEDIATELY
#define log_flush()
#endif

/* Pause the execution after a fixed duration.
 *  duration: Duration to sleep. (Unit: lptimer ticks)
 */
void comm_task_sleep_lp(uint64_t duration);

/* Pause the execution until a fixed point in time.
 *  time: Time to wake up. (Unit: lptimer ticks)
 */
void comm_task_sleep_until_lp(uint64_t time);

/* Pause the execution using the hstimer until a fixed point in time.
 *  time: Time to wake up. (Unit: hstimer ticks)
 */
void comm_task_sleep_until_hs(uint64_t time);

/* Stop the sleep timer. */
void comm_task_end_sleep();

/* Pause the execution of the communication task until a callback unblocks it. */
void comm_task_block();

/* Unblock the execution of the communication task. */
void comm_task_unblock_callback();

/* Update the duty cycle counters and print statistics. */
void comm_task_update_dc();


/* --- macros --- */

/* Macros indicating the current state */
#ifndef STATE_BOOTSTRAPPING_IND
#define STATE_BOOTSTRAPPING_IND()
#endif

#ifndef STATE_JOINING_IND
#define STATE_JOINING_IND()
#endif

#ifndef STATE_RECEIVING_IND
#define STATE_RECEIVING_IND()
#endif

#ifndef STATE_TRANSMITTING_IND
#define STATE_TRANSMITTING_IND()
#endif

#ifndef STATE_NORMAL_IND
#define STATE_NORMAL_IND()
#endif

/* Macro indicating that a Data dissemination slot is active. */
#ifndef SLOT_TYPE_DATA_DISSEMINATION_ON_IND
#define SLOT_TYPE_DATA_DISSEMINATION_ON_IND()
#define SLOT_TYPE_DATA_DISSEMINATION_OFF_IND()
#endif

/* Macro indicating that a Schedule negotiation slot is active. */
#ifndef SLOT_TYPE_SCHEDULE_NEGOTIATION_ON_IND
#define SLOT_TYPE_SCHEDULE_NEGOTIATION_ON_IND()
#define SLOT_TYPE_SCHEDULE_NEGOTIATION_OFF_IND()
#endif

/* Macro indicating that a Schedule distribution slot is active. */
#ifndef SLOT_TYPE_SCHEDULE_DISTRIBUTION_ON_IND
#define SLOT_TYPE_SCHEDULE_DISTRIBUTION_ON_IND()
#define SLOT_TYPE_SCHEDULE_DISTRIBUTION_OFF_IND()
#endif

#endif /* __TASK_COMM_H */
