/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */


/*
 * Schedule
 *
 * This file contains the function used to determine the type and start time of the next used slot in the schedule.
 *
 */

#ifndef __SCHEDULE_H
#define __SCHEDULE_H


/* --- definitions --- */

#define SCHEDULE_ENABLE_DIRECT_TRANSFER   0

#ifndef SD_SCHEDULE_COMPRESS
#define SD_SCHEDULE_COMPRESS              0
#endif


/* --- function prototypes --- */

/* Get the slot information of the next used slot
 *  schedule:      The schedule assigning a sender to a specific Data dissemination slot
 *  starting_time: Input:  The earliest starting time of the next slot. (Unit: lptimer ticks)
 *                 Output: The starting time of the next used slot. (Unit: lptimer ticks)
 *
 *  A slot without a sender is considered a Sleep slot. */
slot_info_t  get_next_used_slot_information(schedule_t* schedule, const schedule_mask_t* schedule_mask, uint64_t* starting_time);

/* Computes the next schedule based on the given requests. */
bool         schedule_compute(comm_state_t* state, int8_t* requests);

/* Packs a full-length schedule into a SD packet using compression. */
bool         schedule_pack(const schedule_t* schedule, schedule_distribution_packet_t* packet);

/* Locally updates the schedule and the schedule version. */
bool         schedule_update(comm_state_t* state, schedule_distribution_packet_t* packet);

/* Returns whether no node is currently scheduled. */
bool         is_schedule_empty(const schedule_t* schedule);

/* Get the number of currently scheduled slots for the given node ID. */
uint8_t      schedule_get_nr_slots(comm_state_t* state, node_id_t node_id);

/* Update the locally desired number of slots so it can be requested in the next epoch. */
slot_count_t request_update(comm_state_t* state);

#endif /* __SCHEDULE_H */
