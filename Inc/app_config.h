/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */


/*
 * Hydra config
 *
 */

#ifndef __APP_CONFIG_H
#define __APP_CONFIG_H


/*** General Parameters **************************************************/

#define FW_NAME                         "DPP2Hydr"  /* max. 8 chars */
#define FW_VERSION_MAJOR                1           /* 0..6 */
#define FW_VERSION_MINOR                0           /* 0..99 */
#define FW_VERSION_PATCH                0           /* 0..99 */

/* This define indicates whether the code is running on FlockLab or not. */
#define FLOCKLAB                                1

/* Set the Node ID. */
#if FLOCKLAB
#define NODE_ID                                 ((uint8_t)FLOCKLAB_NODE_ID)
#define HYDRA_FL_NODE_LIST                      1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16, 19, 20, 21, 22, 23, 24, 26, 27, 28, 29, 31, 32  /* nodes to use on FlockLab */
#else /* FLOCKLAB */
#define NODE_ID                                 1
#endif /* FLOCKLAB */

/* Maximum number of nodes (<= 127) */
#define MAX_NR_NODES                            23

/* Low-power mode to use between rounds during periods of inactivity */
#define LOW_POWER_MODE                          LP_MODE_STOP2

/* Set to 1 to disable GPIO clocks in low-power mode (-> no GPIO tracing possible). */
#define LPM_DISABLE_GPIO_CLOCKS                 0


/*** Evaluation **************************************************/

/* If SKIP_SCHEDULE_NEGOTIATION_ENABLE is true, then the SN phase is skipped when SKIP_SCHEDULE_NEGOTIATION_PIN_VALUE is high. */
#define SKIP_SCHEDULE_NEGOTIATION_ENABLE        0
#define SKIP_SCHEDULE_NEGOTIATION_PIN_VALUE     PIN_GET(FLOCKLAB_SIG1)

/* If DROP_SCHEDULE_NEGOTIATION_ENABLE is true, then packets in the SN phase are dropped with probability DROP_SCHEDULE_NEGOTIATION_PROBABILITY when DROP_SCHEDULE_NEGOTIATION_PIN_VALUE is high. */
#define DROP_SCHEDULE_NEGOTIATION_ENABLE        0
#define DROP_SCHEDULE_NEGOTIATION_PIN_VALUE     PIN_GET(FLOCKLAB_SIG1)
#define DROP_SCHEDULE_NEGOTIATION_PROBABILITY   50

/* If DROP_NODE_PACKETS_ENABLE is true, then all packets which fulfill DROP_NODE_PACKETS_FROM_ID are dropped with probability DROP_NODE_PACKETS_PROBABILITY when DROP_NODE_PACKETS_PIN_VALUE is high. */
#define DROP_NODE_PACKETS_ENABLE                0
#define DROP_NODE_PACKETS_PIN_VALUE             PIN_GET(FLOCKLAB_SIG1)
#define DROP_NODE_PACKETS_PROBABILITY           0
#define DROP_NODE_PACKETS_FROM_ID(x)            (true)

/* If CHANGE_TX_POWER_ENABLE is true, then the TX power of the SN slots is set depending on whether CHANGE_TX_POWER_PIN_VALUE is high or low.
 *  TX_POWER_NORMAL:                TX power when the state is STATE_NORMAL or STATE_JOINING
 *  TX_POWER_BOOTSTRAPPING:         TX power when the state is STATE_BOOTSTRAPPING
 *  CHANGE_TX_POWER_PIN_HIGH:       TX power when CHANGE_TX_POWER_PIN is high
 */
#define CHANGE_TX_POWER_ENABLE                  0
#define CHANGE_TX_POWER_PIN_VALUE               PIN_GET(FLOCKLAB_SIG1)
#define CHANGE_TX_POWER_PIN_HIGH                RADIO_MIN_POWER

/* If CHANGE_FRQ_BAND_ENABLE is true, then the used radio band is set depending on whether CHANGE_FRQ_BAND_PIN_VALUE is high or low.
 *  FRQ_BAND_NORMAL:               Radio band when the state is STATE_NORMAL or STATE_JOINING
 *  FRQ_BAND_BOOTSTRAPPING:        Radio band when the state is STATE_BOOTSTRAPPING
 *  CHANGE_FRQ_BAND_PIN_HIGH:      Radio band when the pin is high
 */
#define CHANGE_FRQ_BAND_ENABLE                  0
#define CHANGE_FRQ_BAND_PIN_VALUE               PIN_GET(FLOCKLAB_SIG1)
#define CHANGE_FRQ_BAND_PIN_HIGH                48

/* If DELAY_START_ENABLE is true, then the node will wait upon startup until the DELAY_START_PIN_VALUE is low. */
#define DELAY_START_ENABLE                      0
#define DELAY_START_PIN_VALUE                   PIN_GET(FLOCKLAB_SIG1)

/* If SKIP_BOOTSTRAP is true, the bootstrapping will be skipped. */
#define SKIP_BOOTSTRAP                          0

/* If SKIP_BOOTSTRAP and SET_INIT_MEMBER_FLAGS are set, then the node starts in STATE_NORMAL with the member flags INIT_MEMBER_FLAGS.
 * INIT_MEMBER_FLAGS needs to be an array of size ceil(MAX_NR_NODES / 8).
 * A node with ID X is in the network, if the ( (X - 1) % 8)-th bit of the ( (X - 1) / 8) byte is set.
 * The initial member flag is ignored if the flag of the node itself is not set or if it is the only flag which is set.
 * If the member flag is set, then the first slot in the schedule is set to NODE_IN_SCHEDULE. */
#define SET_INIT_MEMBER_FLAGS                   1

#ifdef SET_INIT_MEMBER_FLAGS
#if MAX_NR_NODES == 23
#define INIT_MEMBER_FLAGS                       ((uint8_t[]){0b11111111, 0b11111111, 0b01111111, 0b00000000})
#elif MAX_NR_NODES == 12
#define INIT_MEMBER_FLAGS                       ((uint8_t[]){0b11111111, 0b00001111, 0b00000000, 0b00000000})
#elif MAX_NR_NODES == 5
#define INIT_MEMBER_FLAGS                       ((uint8_t[]){0b00011111, 0b00000000, 0b00000000, 0b00000000})
#else
#error "Incorrect number of nodes"
#endif /* MAX_NR_NODES */
#define NODE_IN_SCHEDULE                        1
#endif /* SET_INIT_MEMBER_FLAGS */

/* Upon startup, the node waits for this many lptimer ticks before continuing.
 * This can for example be used to the delay the startup such that the bootstrapping phase is covered in GPIO tracing.
 * This sleep is performed before waiting for DELAY_START_PIN_VALUE. */
#define STARTUP_SLEEP_LP                        0


/*** Communication Parameters **************************************************/

/* Number of rounds per epoch */
#define NR_ROUNDS_EPOCH                         3

/* Number of Data dissemination slots per round */
#define NR_SLOTS_DD                             80

/* Number of Schedule negotiation slots per round */
#define NR_SLOTS_SN                             5

/* Number of Schedule distribution slots per round */
#define NR_SLOTS_SD                             1

/* Number of Sleep slots per round */
#define NR_SLOTS_SL                             (100 - NR_SLOTS_DD - NR_SLOTS_SN - NR_SLOTS_SD)

/* Slot duration. (Unit: lptimer ticks) */
#define SLOT_DURATION_LP                        LPTIMER_MS_TO_TICKS(30)

/* The radio modulation, transmit power (in dBm), and frequency band that are used by the protocol (see radio_constants.c in the Flora lib). */
#define USED_RADIO_MODULATION                   10

#define TX_POWER_NORMAL                         7
#define TX_POWER_BOOTSTRAPPING                  7

#if CHANGE_TX_POWER_ENABLE
#define USED_RADIO_TX_POWER                     (CHANGE_TX_POWER_PIN_VALUE ? CHANGE_TX_POWER_PIN_HIGH : TX_POWER_NORMAL)
#else
#define USED_RADIO_TX_POWER                     TX_POWER_NORMAL
#endif /* CHANGE_TX_POWER_ENABLE */

#define FRQ_BAND_NORMAL                         51
#define FRQ_BAND_BOOTSTRAPPING                  43

#if CHANGE_FRQ_BAND_ENABLE
#define USED_RADIO_FRQ_BAND                     (CHANGE_FRQ_BAND_PIN_VALUE ? CHANGE_FRQ_BAND_PIN_HIGH : FRQ_BAND_NORMAL)
#else
#define USED_RADIO_FRQ_BAND                     FRQ_BAND_NORMAL
#endif /* CHANGE_FRQ_BAND_ENABLE */


/* Minimum number of data slots that the node requests */
#define RQ_NR_DATA_SLOTS_MIN                    3

/* Maximum number of additional data slots that the node requests */
#define RQ_NR_DATA_SLOTS_MAX                    3

/* The payload size of the Data dissemination packet in bytes */
#define DD_PAYLOAD_LENGTH                       (18 - DD_HEADER_LENGTH)

/* Packets from the wrong network will be filtered out if this value is true. */
#define FILTER_PACKETS_FROM_WRONG_NETWORK       0

/* Packets from the correct network but from the wrong sender will be filtered out if this value is true. */
#define FILTER_PACKETS_FROM_WRONG_SENDER        1


/* C_join: Number of times information from a node must be obtained during an epoch to join */
#define NR_ROUNDS_TO_JOIN                       NR_ROUNDS_EPOCH

/* C_stay: Number of times information from a node must be obtained during an epoch to stay */
#define NR_ROUNDS_TO_STAY                       1

/* E_max: Number of times no connectivity to a network must have been determined to switch to bootstrapping */
#define NR_EPOCHS_TO_LEAVE                      3


/* Whether to compress the schedule to a maximum number of bits per node */
#define SD_SCHEDULE_COMPRESS                    1

/* Number of bits to compress. Notice that ID 0 is reserved for an unallocated slot. */
#define SD_SCHEDULE_COMPRESSION_BITS            5

/* Number of bytes used when distributing the schedule */
#define SD_PACKET_NR_SCHEDULE_BYTES             (SD_SCHEDULE_COMPRESS ? ( (NR_SLOTS_DD * SD_SCHEDULE_COMPRESSION_BITS + 7) / 8) : NR_SLOTS_DD)


/*** Task: Communication **************************************************/

/* If the time left to wait until the next slot is bigger than this constant, then the lptimer is used to time the wakeup. (Unit: lptimer ticks) */
#define SLEEP_THRESHOLD_LP                      LPTIMER_MS_TO_TICKS(1)

/* A busy loop is used instead of the hstimer if the amount of time to sleep is smaller than this constant.  (Unit: hstimer ticks) */
#define SLEEP_THRESHOLD_HS                      HS_TIMER_US_TO_TICKS(500)

/* If the time left until the next used slot is bigger than this constant, then the log is flushed. (Unit: lptimer ticks) */
#define LOG_FLUSH_THRESHOLD_LP                  LPTIMER_MS_TO_TICKS(200)

/* Print a warning in case flushing got dangerously close to not finishing in time. (Unit: lptimer ticks) */
#define LOG_FLUSH_WARN_THRESHOLD_LP             LPTIMER_MS_TO_TICKS(50)

/* Time reserved to determine the information of the next slot. (Unit: lptimer ticks) */
#define MAX_SCHEDULE_LOOKUP_TIME_LP             LPTIMER_US_TO_TICKS(100)


/*** Protocol: Bootstrapping **************************************************/

/* Probability of listening on boot channel during bootstrapping */
#define BS_PROBABILITY_BOOT_CH                  80

/* Maximal clock offset during bootstrapping */
#define BS_CLOCK_OFFSET_MAX_HS                  LPTIMER_TICKS_TO_HS_TIMER(NR_SLOTS_DD * SLOT_DURATION_LP / 2)

/* Probability that a joining node will transmit after a successful reception */
#define BS_PROBABILITY_CHAOS_TX                 33


/*** Protocol: Gloria **************************************************/

/* Maximum number of retransmissions used in the Gloria floods */
#define GLORIA_MAX_RETRANSMISSIONS              3

/* Maximum duration of the Gloria floods. (Unit: lptimer ticks) */
#define GLORIA_MAX_DURATION_LP                  LPTIMER_MS_TO_TICKS(20)

/* Duration which a sender should wait before broadcasting. (Unit: lptimer ticks) */
#define GLORIA_GUARD_TIME_LP                    LPTIMER_US_TO_TICKS(250)

/* Duration which a receiver should start listening early in the first Data dissemination slot. (Unit: lptimer ticks) */
#define GLORIA_GUARD_TIME_LONG_LP               LPTIMER_MS_TO_TICKS(5)

/* The maximum time required to prepare the Schedule distribution slot. (Unit: lptimer ticks) */
#define SD_INIT_TIME_LP                         LPTIMER_MS_TO_TICKS(3)


/* The radio modulation, transmit power (in dBm), and frequency band that are used by Gloria (see radio_constants.c in the Flora lib). */
#define GLORIA_INTERFACE_MODULATION             USED_RADIO_MODULATION
#define GLORIA_INTERFACE_POWER                  TX_POWER_NORMAL
#define GLORIA_INTERFACE_RF_BAND                FRQ_BAND_NORMAL

/* Append a timestamp at the end of every Gloria packet. This is necessary to determine the clock offset of discovered neighbours. */
#define GLORIA_INTERFACE_APPEND_TIMESTAMP       1

/* When stopping Gloria, wait for the last transmission to finish. */
#define GLORIA_INTERFACE_WAIT_TX_FINISHED       1


/*** Protocol: Chaos **************************************************/

/* The number of bits used to encode the requested change of assigned number of slots of a node.
 * Needs to be at least 2 and at most 8. One bit will be used for the sign. */
#define SN_NR_DELTA_BITS                        2

/* Bool indicating whether the request value of all the nodes should be printed to the console. The log level needs to be LOG_LEVEL_VERBOSE. */
#define SN_LOG_REQUEST_VALUES                   0

/* Defines whether an additional CRC32 should be added to the SN packet to detect corrupted requests. */
#define SN_ADD_PAYLOAD_CRC                      1

/* Bool indicating whether the CRC value of the packets received during the SN phase should be logged. */
#define SN_LOG_PACKET_CRC                       0


/* The radio modulation, transmit power (in dBm), and frequency band that are used by Chaos (see radio_constants.c in the Flora lib). */
#define CHAOS_INTERFACE_MODULATION              USED_RADIO_MODULATION
#define CHAOS_INTERFACE_POWER                   TX_POWER_NORMAL
#define CHAOS_INTERFACE_RF_BAND                 FRQ_BAND_NORMAL

/* The maximum duration of one Chaos aggregation */
#define CHAOS_DURATION_MAX_LP                   LPTIMER_MS_TO_TICKS(125)

/* Time reserved to prepare for the Chaos slot */
#define CHAOS_SLOT_START_OFFSET_HS              HS_TIMER_MS_TO_TICKS(5)

/* The sending period of Chaos messages inside a Chaos slot. (Unit: hstimer ticks) */
#define CHAOS_MESSAGE_INTERVAL_HS               HS_TIMER_US_TO_TICKS(3500)

/* The minimum time left to restart the receiver if a message was discarded. (Unit: hstimer ticks) */
#define CHAOS_RADIO_MIN_RECEIVE_TIME_HS         HS_TIMER_US_TO_TICKS(1800)

/* Duration a node should wait for a message in STATE_NORMAL before giving up. (Unit: hstimer ticks) */
#define CHAOS_RADIO_RECEIVE_TIMEOUT_HS          HS_TIMER_US_TO_TICKS(2000)

/* Duration a node should wait for a message in STATE_JOINING before giving up. (Unit: hstimer ticks) */
#define CHAOS_RADIO_RECEIVE_TIMEOUT_LONG_HS     LPTIMER_TICKS_TO_HS_TIMER(CHAOS_DURATION_MAX_LP / 4)

/* The time reserved to prepare for the next slot. (Unit: hstimer ticks) */
#define CHAOS_RADIO_TIME_BUFFER_HS              HS_TIMER_US_TO_TICKS(350)

/* The duration a node should start receiving earlier than it would start sending. (Unit: hstimer ticks) */
#define CHAOS_RADIO_RECEIVE_OFFSET_HS           HS_TIMER_US_TO_TICKS(25)


/*** Drift Compensation **************************************************/

/* The maximum drift which is considered correct */
#define DRIFT_MAX_PPM                           40

/* When updating the drift compensation factor, not the calculated value "comp_value" is taken directly, but rather
 * new_value = DRIFT_COMPENSATION_CHANGE * comp_value + (1 - DRIFT_COMPENSATION_CHANGE) * old_value. */
#define DRIFT_COMPENSATION_CHANGE               0.25

/* A drift measurement is only considered valid if the measurement duration is bigger than this constant. (Unit: hstimer ticks) */
#define DRIFT_MEASUREMENT_TIME_MIN_HS           (NR_SLOTS_DD * LPTIMER_TICKS_TO_HS_TIMER(SLOT_DURATION_LP))

/* The minimum and maximum observed network drift are included into the SN packet in order to be able to prevent
 * the drift of the network to consistently drift in one direction due to feedback loops.
 * The computed drift compensation is included as a difference from one.
 * This value is then multiplied by this constant and converted to a 16-bit integer to decrease the size of the SN packet. */
#define SN_PACKET_DRIFT_SCALE                   100000000


/*** Memory **************************************************/

/* The stack size of the individual tasks in # of 4-byte words */
#define BOLT_TASK_STACK_SIZE                    256
#define COMM_TASK_STACK_SIZE                    512

/* Maximal number of messages to read from BOLT at once */
#define BOLT_MAX_READ_COUNT                     100


/*** Flora Library **************************************************/

/* Enable the drift compensation, which is required for the time synchronisation. */
#define HS_TIMER_COMPENSATE_DRIFT               1

/* Do not initialise the hstimer from the RTC. */
#define HS_TIMER_INIT_FROM_RTC                  0

/* Reset the watchdog upon a lptimer counter overflow and when the lptimer expires. */
#define LPTIMER_RESET_WDG_ON_OVF                1
#define LPTIMER_RESET_WDG_ON_EXP                1
#define LPTIMER_CHECK_EXP_TIME                  1

/* Not used, set to 1 byte to reduce memory usage. */
#define UART_FIFO_BUFFER_SIZE                   1

/* Disable the command line interface since it is not used. */
#define CLI_ENABLE                              0

/* Disable logging using SWO. */
#define SWO_ENABLE                              0

/* Disable the BOLT interface. */
#define BOLT_ENABLE                             0

/* Use a software timeout instead of the ones of the radio chip. */
#define RADIO_USE_HW_TIMEOUT                    0


/*** Logging **************************************************/

/* If true, some debug logs are printed during the operation. */
#define LOG_ENABLE                              1

/* Parameter indicating how verbose the logging should be
 * Possible values:
 *   LOG_LEVEL_VERBOSE
 *   LOG_LEVEL_INFO
 *   LOG_LEVEL_WARNING
 *   LOG_LEVEL_ERROR
 *   LOG_LEVEL_QUIET
 */
#define LOG_LEVEL                               LOG_LEVEL_VERBOSE

/* Bool indicating whether the debug output should be printed immediately (using UART) or
 * whether it should be buffered and then later be printed by the debug task to avoid causing timing issues. */
#define LOG_PRINT_IMMEDIATELY                   0

/* The size of the buffer used store the debug output before printing it. This is only used if LOG_PRINT_IMMEDIATELY is zero. */
#define LOG_BUFFER_SIZE                         32768

/* Maximum logging time until the log is discarded */
#ifdef LOG_FLUSH_THRESHOLD_LP
#define UART_TX_TIMEOUT_MS                      LPTIMER_TICKS_TO_MS(LOG_FLUSH_THRESHOLD_LP)
#endif


/*** FlockLab **************************************************/
#if FLOCKLAB

/* Trace the occurrence of interrupts */
//#define ISR_ON_IND()                           PIN_TOGGLE(FLOCKLAB_INT1)
//#define ISR_OFF_IND()                          PIN_TOGGLE(FLOCKLAB_INT1)

/* Trace sending and receiving data */
#define RADIO_TX_START_IND()                     PIN_SET(FLOCKLAB_LED3)
#define RADIO_TX_STOP_IND()                      PIN_CLR(FLOCKLAB_LED3)
#define RADIO_RX_START_IND()                     PIN_SET(FLOCKLAB_LED2)
#define RADIO_RX_STOP_IND()                      PIN_CLR(FLOCKLAB_LED2)

/* Trace the network state of the node */
#define FL_TRACE_NETWORK_STATE                   0
#define FL_TRACE_SCHEDULING                      0

#if FL_TRACE_NETWORK_STATE
#define STATE_BOOTSTRAPPING_IND()                 PIN_SET(FLOCKLAB_INT1); PIN_CLR(FLOCKLAB_INT2)
#define STATE_JOINING_IND()                       PIN_CLR(FLOCKLAB_INT1); PIN_SET(FLOCKLAB_INT2)
#define STATE_NORMAL_IND()                        PIN_SET(FLOCKLAB_INT1); PIN_SET(FLOCKLAB_INT2)
#elif FL_TRACE_SCHEDULING
#define STATE_BOOTSTRAPPING_IND()                 PIN_SET(FLOCKLAB_INT1); PIN_CLR(FLOCKLAB_INT2)
#define STATE_RECEIVING_IND()                     PIN_CLR(FLOCKLAB_INT1); PIN_SET(FLOCKLAB_INT2)
#define STATE_TRANSMITTING_IND()                  PIN_SET(FLOCKLAB_INT1); PIN_SET(FLOCKLAB_INT2)
#else
#define SLOT_TYPE_DATA_DISSEMINATION_ON_IND()     PIN_SET(FLOCKLAB_INT1); PIN_CLR(FLOCKLAB_INT2)
#define SLOT_TYPE_SCHEDULE_NEGOTIATION_ON_IND()   PIN_CLR(FLOCKLAB_INT1); PIN_SET(FLOCKLAB_INT2)
#define SLOT_TYPE_SCHEDULE_DISTRIBUTION_ON_IND()  PIN_SET(FLOCKLAB_INT1); PIN_SET(FLOCKLAB_INT2)
#define SLOT_TYPE_DATA_DISSEMINATION_OFF_IND()    PIN_CLR(FLOCKLAB_INT1); PIN_CLR(FLOCKLAB_INT2)
#define SLOT_TYPE_SCHEDULE_NEGOTIATION_OFF_IND()  PIN_CLR(FLOCKLAB_INT1); PIN_CLR(FLOCKLAB_INT2)
#define SLOT_TYPE_SCHEDULE_DISTRIBUTION_OFF_IND() PIN_CLR(FLOCKLAB_INT1); PIN_CLR(FLOCKLAB_INT2)
#endif /* FL_TRACE_NETWORK_STATE */

#endif /* FLOCKLAB */


/*** Parameter Checks **************************************************/

#if BOLT_ENABLE && (BOLT_MAX_MSG_LEN < DPP_MSG_PKT_LEN)
#error "BOLT_MAX_MSG_LEN is too small"
#endif

#if SN_NR_DELTA_BITS < 2
#error "SN_NR_DELTA_BITS needs to be at least 2"
#endif

#if SN_NR_DELTA_BITS > 8
#error "SN_NR_DELTA_BITS can be at most 8"
#endif

#if (SKIP_SCHEDULE_NEGOTIATION_ENABLE + DROP_SCHEDULE_NEGOTIATION_ENABLE + DROP_NODE_PACKETS_ENABLE + CHANGE_TX_POWER_ENABLE + CHANGE_FRQ_BAND_ENABLE + DELAY_START_ENABLE) > 1
#error "Multiple usages defined for SIG1"
#endif

#if (NR_ROUNDS_TO_JOIN > NR_ROUNDS_EPOCH) || (NR_ROUNDS_TO_STAY > NR_ROUNDS_EPOCH)
#error "Can maximally obtain information from a node NR_ROUNDS_EPOCH per epoch"
#endif

#if SD_SCHEDULE_COMPRESS && ( ( (1 << SD_SCHEDULE_COMPRESSION_BITS) - 1) < MAX_NR_NODES)
#error "Cannot fit all node IDs for MAX_NR_NODES into the compressed schedule"
#endif

#endif /* __APP_CONFIG_H */
