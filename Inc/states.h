/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * States
 *
 * This file provides the helper functions necessary to switch the network state.
 *
 */

#ifndef __STATES_H
#define __STATES_H


/* --- function prototypes --- */

/* This function causes the node to enter the BOOTSTRAPPING state.
 *  state:       The communication state structure
 *  reset_radio: Whether the radio will be reset
 */
void enter_bootstrapping(comm_state_t* state, bool reset_radio);

/* This function causes the node to enter the JOINING state.
 *  state:       The communication state structure
 */
void enter_joining(comm_state_t* state);

/* This function causes the node to enter the NORMAL state.
 *  state:       The communication state structure
 */
void enter_normal(comm_state_t* state);

/* Returns the currently used TX power based on the state of the node. */
int8_t  states_get_tx_power(const comm_state_t* state);

/* Returns the currently used frequency band based on the state of the node. */
uint8_t states_get_freq_band(const comm_state_t* state);

#endif /* __STATES_H */
