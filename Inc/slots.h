/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Slots
 *
 * This file contains the definition of the structures used by functions implementing the different slot types.
 * It also contains the declaration of these functions and of some helper functions.
 *
 */

#ifndef __SLOTS_SLOTS_H
#define __SLOTS_SLOTS_H


/* --- definitions --- */

/* The minimum and maximum slot count deltas which can be used in the SN packet. */
#define SLOT_COUNT_DELTA_MAX                  ((1 << (SN_NR_DELTA_BITS - 1) ) - 1)
#define SLOT_COUNT_DELTA_MIN                  (- SLOT_COUNT_DELTA_MAX)
#define SLOT_COUNT_NEUTRAL_ELEMENT            (SLOT_COUNT_DELTA_MIN - 1)

/* The header size of a Data dissemination packet */
#define DD_HEADER_LENGTH                      ((1 + NR_MEMBER_FLAG_BYTES) * sizeof(uint8_t))

/* The number of bytes used to store the request values in the Schedule negotiation packet. */
#define SN_PACKET_NR_REQUEST_BYTES            ((MAX_NR_NODES * SN_NR_DELTA_BITS + 7) / 8)

/* These values define the border between the three different zones when comparing MIN and MAX values of schedule versions. */
#define SN_COMPARISON_START_ZONE_1            86
#define SN_COMPARISON_START_ZONE_2            171

/* Print the member flags for a network using 4 bytes for its flags */
#if   NR_MEMBER_FLAG_BYTES == 4
#define PRINT_FLAGS(identifier, flags)        LOG_VERBOSE("Member flags - %15s: %08lu | %08lu | %08lu | %08lu", identifier, BINARY(flags[3]), BINARY(flags[2]), BINARY(flags[1]), BINARY(flags[0]))
#elif NR_MEMBER_FLAG_BYTES == 3
#define PRINT_FLAGS(identifier, flags)        LOG_VERBOSE("Member flags - %15s: %08lu | %08lu | %08lu", identifier, BINARY(flags[2]), BINARY(flags[1]), BINARY(flags[0]))
#elif NR_MEMBER_FLAG_BYTES == 2
#define PRINT_FLAGS(identifier, flags)        LOG_VERBOSE("Member flags - %15s: %08lu | %08lu", identifier, BINARY(flags[1]), BINARY(flags[0]))
#elif NR_MEMBER_FLAG_BYTES == 1
#define PRINT_FLAGS(identifier, flags)        LOG_VERBOSE("Member flags - %15s: %08lu", identifier, BINARY(flags[0]))
#else
#define PRINT_FLAGS(identifier, flags)
#endif /* NR_MEMBER_FLAG_BYTES */


/* --- typedefs --- */

typedef struct __attribute__((__packed__, __aligned__(1))){
    uint8_t   id_src;
    uint8_t   member_flags[NR_MEMBER_FLAG_BYTES];
    uint8_t   payload[GLORIA_INTERFACE_MAX_PAYLOAD_LEN - DD_HEADER_LENGTH];
} data_dissemination_packet_t;

typedef struct __attribute__((__packed__, __aligned__(1))){
    // Source ID
    uint8_t  id_src;
    // Status flags and debug information - used for alignment
    uint8_t  status;
    // The minimum schedule version present in the network
    uint8_t  sched_vers_min;
    // The maximum schedule version present in the network
    uint8_t  sched_vers_max;
    // One bit for every node indicating whether the node is a part of the network
    uint8_t  member_flags[NR_MEMBER_FLAG_BYTES];
    // SN_NR_DELTA_BITS bits per node indicating the requests
    uint8_t  requests[SN_PACKET_NR_REQUEST_BYTES];
    // The minimum and maximum drift compensation factors computed in the network
    int16_t  drift_min;
    int16_t  drift_max;
#if SN_ADD_PAYLOAD_CRC
    uint32_t crc32;
#endif /* SN_ADD_PAYLOAD_CRC */
} schedule_negotiation_packet_t;

typedef struct __attribute__((__packed__, __aligned__(1))){
    uint8_t    id_src;
    uint8_t    member_flags[NR_MEMBER_FLAG_BYTES];
    schedule_t schedule[SD_PACKET_NR_SCHEDULE_BYTES];
    uint8_t    sched_vers;
} schedule_distribution_packet_t;


/* --- function prototypes --- */

/* Perform a Data dissemination slot. */
void run_data_dissemination_slot(comm_state_t* state);

/* Perform a Schedule negotiation slot. */
void run_schedule_negotiation_slot(comm_state_t* state);

/* Perform a Schedule distribution slot. */
void run_schedule_distribution_slot(comm_state_t* state);

/* Perform the End of Round updates. */
void run_end_of_round(comm_state_t* state);


/* Helper functions */

/* This function extracts the timestamp of a packet.
 *   packet:    Pointer to the packet
 *   size:      The size of the packet
 */
uint64_t extract_timestamp(const uint8_t* packet, uint8_t size);

/* Computes the time offset of the sender to the local clock.
 * Only packets which contain a timestamp in the last 8 bytes are accepted.
 * It is assumed that the first byte of the packet contains a type ID (in the four least significant bits).
 * Packets with unknown Type IDs are ignored.
 *   packet:          Pointer to the packet
 *   size:            Size of the packet
 *   time_offset_hs:  Pointer to a uint64_t, where the resulting offset will be stored. (Unit: hstimer ticks)
 *
 *   Return value: 1 if successful, 0 otherwise
 *                 The time_offset will not be overwritten if 0 is returned
 */
bool get_packet_time_offset(const uint8_t* packet, uint8_t size, int64_t* time_offset_hs);

/* Gets the byte offset of the member flags in the current packet.
 * It is assumed that the first byte of the packet contains a type ID (in the four least significant bits).
 * Packets with unknown Type IDs are ignored.
 *   packet:              Pointer to the packet
 *   size:                Size of the packet
 *   member_flags_offset: Pointer to the member flags
 *
 *   Return value: 1 if successful, 0 otherwise
 *                 The member_flags_offset will not be overwritten if 0 is returned
 */
bool get_member_flags_offset(const uint8_t* packet, uint8_t size, const uint8_t* member_flags_offset);

/* Returns the number of set flags. */
uint8_t get_nr_of_contributors(const uint8_t* flags);

/* Determines whether the node should join another network.
 *   member_flags_rcvd:  Member flags of the received packet
 *   member_flags_local: Member flags of the local node
 */
bool is_dominant_network(const uint8_t* member_flags_rcvd, const uint8_t* member_flags_local);

/* Checks if the given member flags contain only zeros.
 *   member_flag: The flags to check
 */
bool is_member_flag_empty(const uint8_t* member_flag);

#endif /* __SLOTS_SLOTS_H */
